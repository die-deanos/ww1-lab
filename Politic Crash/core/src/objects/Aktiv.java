package objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.PoliticCrash.StatusRunde;

public class Aktiv extends Skill {

	//Neue allgemeine Variablen.
	protected int risiko; //Chance auf Fehlschlag.
	protected int maxrunde; //Variable für Skills welche erst nach x-Runden Wirkung zeigen.
	protected int dauer; //Variable für Skills welche jede Runde etwas zurückgeben.
	protected boolean eigen; //Gibt an ob für eigenes Land oder andere Länder anwendbar.
	protected Sprite button; //Button zum ausführen im Ländermenü.
	protected StatusRunde status; //Variable für den aktuellen Status des Skills.
	protected int anwendungsKosten=0; //nur selten verwendet, deswegen nicht im Konstruktor und standartmäßig auf 0

	//Anwendungsspezifisch, wird bei aufruf übergeben / resettet.
	protected Land target; //Land, welches als Ziel angegeben ist.
	protected int runde = 0; //interner Rundenzähler. 0 = nicht aktiv.

	public Aktiv(String name, String beschreibung, Land own, Skill parent, int maxlvl, int[] kosten, int risiko, int maxrunde, int dauer, boolean eigen) {
		super(name, beschreibung, own, parent, maxlvl, kosten);
		passiv=false;
		this.risiko = risiko;
		this.maxrunde = maxrunde;
		this.dauer = dauer;
		this.eigen = eigen;
		button = new Sprite(new Texture("InGame\\Skilltree\\Button.png"));
	}

	public void initialisieren(Land target) {
		runde = maxrunde+dauer+1; //Runde erhält nur maxrunde oder dauer, gibt somit an wann der Skill seine Wirkung zeigt.
		this.target = target;
		status = StatusRunde.warten; //warten = der Skill ist aktiv, muss aber noch x-Runden warten bevor er seine Wirkung zeigt.
		runde();
	}

	public boolean runde() { //Rundenbasierende Zählermethode.
		runde--; //Bei jeden Aufruf wird die Rundenanzahl verringert --> Hauptfunktion des Rundenzählers.

		if (runde == dauer) { //dauer entfalltet jede Runde seine Wirkung.
			status = StatusRunde.erfolg; //lebendig = der Skill zeigt seine Wirkung.
			if(dauer!=0) Nachrichtsystem.neueInfo(toString()); //Nachricht feuern wenn Skill eine wirkungsdauer hat
			doSomething(this.target); //Aufruf der 'Wirkungs-Methode' des Skills.
		}

		if (runde == 0){ //Alle Phasen zu Ende.
			Nachrichtsystem.neueInfo(toString()); //Nachricht feuern
			runde = 0; target = null; status = StatusRunde.inaktiv; //Skill resetten
			return false; //ruft Entfernung aus der List in InGame auf
		}
		return true; //Skill läuft normal weiter
	}

	public void doSomething(Land target) { //Gewährt Zugriff auf die Wirkungsmethode des eigentlichen Skills.
	}

	public int getAnwendungsKosten() {
		return anwendungsKosten;
	}

	public int getRisiko() {
		return risiko;
	}

	public int getMaxRunde() {
		return maxrunde;
	}

	public int getDauer() {
		return dauer;
	}

	public boolean getImEinsatz() { //Gibt zurück ob der Skill gerade seine Wirkung zeigt.
		if (status == StatusRunde.erfolg) return true; //Skill zeigt seine Wirkung.
		else return false; //Skill zeigt keine Wirkung.
	}

	public boolean getEigen() { //Gibt zurück ob der Skill auf das eigene Land, oder ein Gegner-Land eingesetzt werden kann.
		return eigen;
	}

	public Sprite getButton() { //Gibt den Knopf zurück zum Starten der Methode.
		return button;
	}
}
