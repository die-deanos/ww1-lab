package objects.aktionen;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;

public class Schmeicheln extends Aktiv {

    private int gewinn;

    public Schmeicheln(Land own) {
        super(  "Schmeicheln",
                "Belobigt den Anführer. Chance auf Informationen.",
                own,
                null,
                1,
                new int[]{0},
                50,
                1,
                0,
                false);
    }

    @Override
    public void doSomething(Land target) {
        gewinn=20;
        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko) {
            own.informationenAendern(gewinn,target);
            status = PoliticCrash.StatusRunde.erfolg;
        }
        else status = PoliticCrash.StatusRunde.fehlschlag;
    }

    @Override
    public String toString() {
        String s1, s2;
        if (own.equals(Land.spieler)) {
            s1 = "Deine Schmeicheleien";
            s2 = "du konntest ";
        }
        else {
            s1 = "Die Schmeicheleien von "+own.getName();
            s2 = own.getName()+" konnte ";
        }
        switch (status) {
            case erfolg:
                ausgabe = s1+" hatten Erfolg, "+s2+gewinn+" Informationen von " + target.getName() + " erhalten.";
                break;
            case fehlschlag:
                ausgabe = s1+" blieben erfolglos, "+s2+"keinerlei Informationen von " + target.getName() + " erhalten.";
                break;
        }
        return super.toString();
    }
}
