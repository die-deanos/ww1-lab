package objects.aktionen;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;

public class Volksrede extends Aktiv {

    int gewinn=5;

    public Volksrede(Land own) {
        super(  "Volksrede",
                "Halte eine Rede zu deinem Volk um das Vertrauen der Bevölkerung möglicherweise zu stärken",
                own,
                null,
                1,
                new int[]{0},
                50,
                1,
                0,
                true
        );
    }

    @Override
    public void doSomething(Land target) {
        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko) {
            own.vertrauenAendern(gewinn,target);
            status = PoliticCrash.StatusRunde.erfolg;
        }
        else status = PoliticCrash.StatusRunde.fehlschlag;
    }

    @Override
    public String toString() {
        switch (status) {
            case erfolg:
                if (own.equals(Land.spieler)) ausgabe = "Du konntest durch eine Volksrede dein inneres Vertrauen um "+gewinn+" Prozent stärken.";
                else ausgabe = own.getName()+" konnte durch eine Volksrede sein inneres Vertrauen um "+gewinn+" Prozent stärken.";
                break;
            case fehlschlag:
                if (own.equals(Land.spieler)) ausgabe = "Deine Volksrede schlug fehl, du konntest dein Vertrauen nicht stärken.";
                else ausgabe = "Die Volksrede von "+own.getName()+" schlug fehl, "+own.getName()+" konnte sein Vertrauen nicht stärken.";
                break;
        }
        return super.toString();
    }
}
