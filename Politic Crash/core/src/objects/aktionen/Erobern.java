package objects.aktionen;

import com.mygdx.game.PoliticCrash.StatusRunde;
import objects.Aktiv;
import objects.Land;

public class Erobern extends Aktiv {

    int ausgabeNr;

    public Erobern(Land own) {
        super(  "Erobern",
                "Nutzt gesammelte Informationen über ein Land und versucht dieses zu übernehmen. Die Chance wird erhöht je weniger Vertrauen die Bevölerung in ihre Regierung hat.",
                own,
                null,
                1,
                new int[]{0},
                0,
                1,
                0,
                false);
       anwendungsKosten=300;
    }

    @Override
    public void doSomething(Land target) {
        int chance = lvl * 10 + (int) ((Math.random() * 100) + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        int v=target.getVertrauen(target);
        if (v<30) {
            if (chance <=5+(30-v)) {
                target.erobern(own);//Wirkung bei erfolg
                status = StatusRunde.erfolg;
            } else {
                status = StatusRunde.fehlschlag; //Fehlschlag
                ausgabeNr = 1;
            }
        } else {
            status = StatusRunde.fehlschlag;
            ausgabeNr = 2;
        }
    }

    @Override
    public String toString() {
        switch(status) {
            case erfolg:
                ausgabe = "Die Eroberung von "+ target.getName() + " war Erfolgreich.";
                break;
            case fehlschlag:
                switch(ausgabeNr) {
                    case 1:
                        ausgabe = "Eroberung fehlgeschlagen! Die Bevölkerung von "+target.getName()+" konnte nicht Konvertiert werden.";
                        break;
                    case 2:
                        ausgabe = "Eroberung fehlgeschlagen! Die Bevölkerung von "+target.getName() + " hat noch zu viel Vertrauen in ihre Regierung.";
                        break;
                }
        }
        return ausgabe; //gibt den entsprechenden Status/Sonderfall des Skills in Form eines Strings zurück. Dieser wird dann im UI ausgegeben.
    }
}
