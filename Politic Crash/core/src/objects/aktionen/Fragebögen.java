package objects.aktionen;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Fragebögen extends Aktiv {

    int gewinn=20;

    public Fragebögen(Land own) {
        super(  "Fragebögen",
                "Starte eine Umfrage um eventuell Informationen über deine Befölkerung zu erhalten",
                own,
                null,
                1,
                new int[]{0},
                50,
                1,
                0,
                true
        );
    }

    @Override
    public void doSomething(Land target) {
        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko) {
            own.informationenAendern(gewinn,target);
            status = PoliticCrash.StatusRunde.erfolg;
        }
        else status = PoliticCrash.StatusRunde.fehlschlag;
    }

    @Override
    public String toString() {
        switch (status) {
            case erfolg:
                if (own.equals(Land.spieler)) ausgabe = "Du konntest "+gewinn+" Informationen aus Fragebögen gewinnen";
                else ausgabe = own.getName()+" konnte "+gewinn+" Informationen aus Fragebögen gewinnen";
                break;
            case fehlschlag:
                if (own.equals(Land.spieler)) ausgabe = "Die Auswertung deiner Fragebögen lieferte keinerlei neue Informationen.";
                else ausgabe = "Die Auswertung der Fragebögen in "+own.getName()+" lieferte dem Land keinerlei neue Informationen.";
                break;
        }
        return super.toString();
    }
}
