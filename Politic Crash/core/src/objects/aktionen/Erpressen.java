package objects.aktionen;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;

import java.util.ArrayList;

public class Erpressen extends Aktiv {

    int lohn;
    Land ziel;

    public Erpressen(Land own) {
        super(  "Erpressen",
                "Tauscht Information aus diesem Land gegen Informationen aus einem anderem zufällig anderen Land.",
                own,
                null,
                1,
                new int[]{0},
                0,
                0,
                0,
                false);
        anwendungsKosten=20;
    }

    @Override
    public void doSomething(Land target) {
        ArrayList<Land> ziele = new ArrayList<Land>();
        lohn = (int) (Math.random() * 101 + 20);

        if(own.getInformationen(target) >= anwendungsKosten) {
            for (Land l : Land.values()) {
                if(target.getInformationen(l) >= lohn) {
                    if(l != target) ziele.add(l);
                }
            }
            if(ziele.size()>0) {
                own.informationenAendern(-anwendungsKosten, target);
                ziel = ziele.get((int) (Math.random()*ziele.size()));
                target.informationenAendern(-lohn, ziel);
                status= PoliticCrash.StatusRunde.erfolg;
            }
            else status = PoliticCrash.StatusRunde.fehlschlag;
        }
    }

    @Override
    public String toString() {
        String s1;
        if (own.equals(Land.spieler)) s1 = "dir ";
        else s1 = own.getName()+" ";

        switch (status) {
            case erfolg:
                ausgabe = target.getName() + " hat " + s1 + lohn + " Informationen von " + ziel + " gegeben.";
                break;
            case fehlschlag:
                ausgabe = target.getName() + " konnte " + s1 + "keine neuen Informationen geben.";
                break;
        }
        return super.toString();
    }
}
