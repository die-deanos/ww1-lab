package objects;

public class Passiv extends Skill{

	public Passiv(String name, String beschreibung, Land own, Skill parent, int maxlvl, int kosten[]) {
		super(name, beschreibung, own, parent, maxlvl, kosten);
		passiv=true;
	}

	public void doSomething() { //vererbte Funktions-Methode.

	}
}
