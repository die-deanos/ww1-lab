package objects;

import java.util.ArrayList;
import java.util.Arrays;

import com.mygdx.game.PoliticCrash.Regierungsform;
import objects.aktionen.*;
import objects.politicskills.autokratie.*;
import objects.politicskills.demokratie.*;
import objects.politicskills.diktatur.*;
import objects.politicskills.kommunismus.*;
import objects.techskills.aktiv.*;
import objects.techskills.passiv.*;

public class Skillbaum {

	private ArrayList<Aktiv> aktiv = new ArrayList<Aktiv>(); //Array Sammelliste für alle Skills welche nach belieben aktiviert werden können.
	private ArrayList<Passiv> passiv = new ArrayList<Passiv>(); //Array Sammelliste für alle Skills welche ab Freischaltung dauerhaft aktiv sind.
	private ArrayList<Skill> politisch = new ArrayList<Skill>(); //Array Sammelliste für alle Skills welche mit der Regierungsform übergeben werden.
	private ArrayList<Skill> technisch = new ArrayList<Skill>(); //Array Sammelliste für alle Skills welche für jedes Land zur Verfügung stehen.

	//Nachfolgend 3 Array Sammellisten welche Skills dem Verhalten der Bots entsprechen.
	private ArrayList<Skill> agressiv = new ArrayList<Skill>();
	private ArrayList<Skill> neutral = new ArrayList<Skill>();
	private ArrayList<Skill> freundlich = new ArrayList<Skill>();

	Regierungsform regierungsform;

	//Nachfolgender Konstruktor instanziiert alle Skills.
	public Skillbaum(Regierungsform r, Land land) {

		//Standartaktionen
		Skill erobern = new Erobern(land);
		erobern.unlock();
		Skill erpressen = new Erpressen(land);
		erpressen.unlock();
		Skill schmeicheln = new Schmeicheln(land);
		schmeicheln.unlock();
		Skill fragebögen = new Fragebögen(land);
		fragebögen.unlock();
		Skill volksrede = new Volksrede(land);
		volksrede.unlock();
		aktiv.addAll(Arrays.asList((Aktiv)erobern,(Aktiv)erpressen,(Aktiv)schmeicheln,(Aktiv)fragebögen,(Aktiv)volksrede));

		//Politische Skills
		regierungsform=r;
		switch (r) { //der Switch-Case speichert alle politischen Skills und weist sie der entsprechenden Regierungsform zu.
			case autokratie:
				Skill papst = new Papst(land);
				Skill koenig = new Koenig(land);
				Skill polygamie = new Polygamie(land, papst, koenig);
				Skill monogamie = new Monogamie(land, papst, koenig);
				Skill erloeser = new Erloeser(land, monogamie, polygamie);
				politisch.addAll(Arrays.asList(papst,koenig,polygamie,monogamie,erloeser));
				aktiv.add((Aktiv) erloeser);
				passiv.addAll(Arrays.asList((Passiv)papst, (Passiv)koenig, (Passiv)polygamie,(Passiv)monogamie));
				break;
			case kommunismus:
				Skill marktwirtschaft = new Marktwirtschaft(land);
				Skill staatswirtschaft = new Staatswirtschaft(land);
				Skill hinrichtung = new Hinrichtung(land, marktwirtschaft, staatswirtschaft);
				Skill umerziehungsanstalt = new Umerziehungsanstalt(land, marktwirtschaft, staatswirtschaft);
				Skill kommunistischesManifest = new KommunistischesManifest(land, hinrichtung, umerziehungsanstalt);
				politisch.addAll(Arrays.asList(marktwirtschaft,staatswirtschaft,hinrichtung,umerziehungsanstalt,kommunistischesManifest));
				aktiv.add((Aktiv) kommunistischesManifest);
				passiv.addAll(Arrays.asList((Passiv)marktwirtschaft, (Passiv)staatswirtschaft,(Passiv)hinrichtung,(Passiv)umerziehungsanstalt));
				break;
			case diktatur:
				Skill waffengewalt = new Waffengewalt(land);
				Skill propaganda = new Propaganda(land);
				Skill einHerrscherSystem = new EinHerrscherSystem(land, propaganda, waffengewalt);
				Skill einparteisystem = new Einparteisystem(land, propaganda, waffengewalt);
				Skill standpunktDesObersten = new StandpunktDesObersten(land, einparteisystem, einHerrscherSystem);
				politisch.addAll(Arrays.asList(waffengewalt,propaganda,einHerrscherSystem,einparteisystem,standpunktDesObersten));
				aktiv.add((Aktiv) standpunktDesObersten);
				passiv.addAll(Arrays.asList((Passiv)waffengewalt, (Passiv)propaganda,(Passiv)einHerrscherSystem,(Passiv)einparteisystem));
				break;
			case demokratie:
				Skill volksentscheid = new Volksentscheid(land);
				Skill wahlen = new Wahlen(land);
				Skill merkeln = new Merkeln(land, wahlen, volksentscheid);
				Skill minderheitsregierung = new Minderheitsregierung(land, wahlen, volksentscheid);
				Skill grundrechte = new Grundrechte(land, minderheitsregierung, merkeln);
				politisch.addAll(Arrays.asList(volksentscheid,wahlen,merkeln,minderheitsregierung,grundrechte));
				aktiv.add((Aktiv) grundrechte);
				passiv.addAll(Arrays.asList((Passiv)volksentscheid, (Passiv)wahlen,(Passiv)merkeln,(Passiv)minderheitsregierung));
				break;
		}

		//Technische Skills
		Skill pc = new Pc(land); //Alle Skills des PC-Baumes.
		Skill freiesinternet = new FreiesInternet(land, pc);
		Skill firewall = new Firewall(land, pc);
		Skill hacken = new Hacken(land, pc);

		Skill funkstelle = new Funkstelle(land); //Alle Skills des Funkstellenbaumes.
		Skill kurzwellensensoren = new Kurzwellensensoren(land, funkstelle);
		Skill langwellensensoren = new Langwellensensoren(land, funkstelle);

		Skill nachrichtendienst = new Nachrichtendienst(land); //Alle Skills des Nachrichtendienstes.
		Skill auslandsnachrichten = new Auslandsnachrichten(land, nachrichtendienst);
		Skill inlandsnachrichten = new Inlandsnachrichten(land, nachrichtendienst);

		Skill fernsehen = new Fernsehen(land); //Alle Skills des Fernsehen-Baumes.
		Skill unterhaltungsprogramm = new Unterhaltungsprogramm(land, fernsehen);
		Skill filter = new Filter(land, fernsehen);

		Skill spione = new Spione(land); //Alle Skills der Spione.
		Skill sicherheitsdienst = new Sicherheitsdienst(land, spione);
		Skill inlandsspione = new Inlandsspione(land, spione);
		Skill auslandsspione = new Auslandsspione(land, spione);

		//Anschließend werden alle Skills jeweils einer passiv/aktiv und, falls technisch, der technisch Array Liste übergeben.
		technisch.addAll(Arrays.asList(
				pc, freiesinternet, firewall, hacken,
				funkstelle, kurzwellensensoren, langwellensensoren,
				nachrichtendienst, auslandsnachrichten, inlandsnachrichten,
				fernsehen, unterhaltungsprogramm, filter,
				spione, sicherheitsdienst, inlandsspione, auslandsspione)); //alle Elemente hinzufügen
		passiv.addAll(Arrays.asList((Passiv) pc, (Passiv) freiesinternet, (Passiv) funkstelle, (Passiv) kurzwellensensoren, (Passiv) nachrichtendienst, (Passiv) auslandsnachrichten, (Passiv) inlandsnachrichten, (Passiv) fernsehen, (Passiv) spione));
		aktiv.addAll(Arrays.asList((Aktiv) firewall, (Aktiv) hacken, (Aktiv) langwellensensoren, (Aktiv) unterhaltungsprogramm, (Aktiv) filter, (Aktiv) sicherheitsdienst, (Aktiv) inlandsspione, (Aktiv) auslandsspione));

		agressiv.addAll(Arrays.asList(inlandsspione, auslandsspione));
		neutral.addAll(Arrays.asList(freiesinternet, langwellensensoren, kurzwellensensoren, filter, pc));
		freundlich.addAll(Arrays.asList(filter, sicherheitsdienst, unterhaltungsprogramm, firewall, fernsehen));
	}
	
	public ArrayList<Aktiv> getAktiv() {
		return aktiv;
	}
	
	public ArrayList<Passiv> getPassiv() {
		return passiv;
	}

	public ArrayList<Skill> getPolitisch() { return politisch; }

	public ArrayList<Skill> getTechnisch() { return technisch; }

	public ArrayList<Skill> getAgressiv() {
		return agressiv;
	}

	public ArrayList<Skill> getNeutral() {
		return neutral;

	}
	public ArrayList<Skill> getFreundlich() {
		return freundlich;
	}

	public Regierungsform getRegierungsform() {
		return regierungsform;
	}
}
