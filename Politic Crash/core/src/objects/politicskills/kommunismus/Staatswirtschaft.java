package objects.politicskills.kommunismus;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Staatswirtschaft extends Passiv {

    public Staatswirtschaft(Land own) {
        super(  "Staatswirtschaft",
                "Der Staat entscheidet den Handel. Steigert stark Informationen pro Runde, senkt aber Vertrauen.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Marktwirtschaft")) {
                Marktwirtschaft m = (Marktwirtschaft) s;
                if(m.getFreigeschaltet()){
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {
        own.vertrauenAendern(-2,own);
        own.informationenAendern(5, own);
    }
}
