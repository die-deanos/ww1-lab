package objects.politicskills.kommunismus;

import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class KommunistischesManifest extends Aktiv {

    private Skill parent2;

    public KommunistischesManifest(Land own, Skill parent, Skill parent2) {
        super(  "Kommunistisches Manifest",
                "Länder mit einem Vertrauenswert von 95% oder besser können durch eine geringe Chance übernommen werden.",
                own,
                parent,
                1,
                new int[]{150},
                90,
                3,
                0,
                false);

        this.parent2 = parent2;
    }

    @Override
    public boolean unlock() {
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething(Land target) {
        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko && !target.getErobertVon(own)) {
            target.erobern(own); //erobert das Land sofort, der ehemalige Besitzer des Landes hat das Spiel verloren.
        }
    }

    public String toString() {
        String s1,s2;
        if (own.equals(Land.spieler)) {
            s1 = "eures Kommunistischen Manifestes";
            s2 = "eure kommunistischen Thesen";
        }
        else {
            s1 = "des Kommunistischen Manifestes von"+own.getName();
            s2 = "die kommunistischen Thesen von"+own.getName();
        }

        switch (status) {
            case erfolg:
                ausgabe = "Nach Studium "+s1+" Kommunistischen Manifestes ist "+target.getName()+" nun Anhänger des Kommunismus";
                break;
            case fehlschlag:
                ausgabe = target.name()+" lehnte "+s2+" kommunistischen Thesen leider ab";
                break;
        }
        return ausgabe;
    }
}
