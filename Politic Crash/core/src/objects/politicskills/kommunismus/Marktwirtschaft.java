package objects.politicskills.kommunismus;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Marktwirtschaft extends Passiv {

    public Marktwirtschaft(Land own) {
        super(  "Marktwirtschaft",
                "Privatorganisationen bestimmten den Handel. Gewährt Vertrauen pro Runde, senkt aber Informationen.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Staatswirtschaft")) {
                Staatswirtschaft st = (Staatswirtschaft) s;
                if(st.getFreigeschaltet()){
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {
        own.informationenAendern(-3,own);
        own.vertrauenAendern(5,own);
    }
}
