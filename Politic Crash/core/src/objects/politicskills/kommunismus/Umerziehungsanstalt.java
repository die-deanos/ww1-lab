package objects.politicskills.kommunismus;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Umerziehungsanstalt extends Passiv {

    private Skill parent2;

    public Umerziehungsanstalt(Land own, Skill parent, Skill parent2) {
        super(  "Umerziehungsanstalt",
                "Halbiert erhaltene Information und verlorenes Vertrauen aus eigenem Land",
                own,
                parent,
                1,
                new int[]{200});

        this.parent2  = parent2;
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Hinrichtung")) {
                Hinrichtung h = (Hinrichtung) s;
                if(h.getFreigeschaltet()){
                    return false;
                }
            }
        }
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething() {
        double[] skill = own.getBonimali();
        skill[1] -= 0.5;
        skill[2] -= 0.5;
        own.setBonimali(skill[0], skill[1], skill[2], skill[3]);
    }
}
