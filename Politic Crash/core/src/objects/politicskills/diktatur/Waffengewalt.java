package objects.politicskills.diktatur;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Waffengewalt extends Passiv {
    public Waffengewalt(Land own) {
        super(  "Waffengewalt",
                "Unterdrückt die Bevölkerung mit Gewalt. Der Vertrauenswert bleibt für andere Länder unsichtbar.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Propaganda")) {
                Propaganda p = (Propaganda) s;
                if(p.getFreigeschaltet()) {
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {

    }
}
