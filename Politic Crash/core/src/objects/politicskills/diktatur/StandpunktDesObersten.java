package objects.politicskills.diktatur;

import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class StandpunktDesObersten extends Aktiv {

    private Skill parent2;
    private int gewinn;

    public StandpunktDesObersten(Land own, Skill parent, Skill parent2) {
        super(  "Standpunkt des Obersten",
                "Vertrauen über 50% wird sofort in Informationsmengen umgewandelt. 1% = 10 Informationen.",
                own,
                parent,
                1,
                new int[]{200},
                0,
                0,
                0,
                true);

        this.parent2 = parent2;
    }

    @Override
    public boolean unlock() {
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething(Land target) {
        //die generierte menge an informationen bitte in gewinn schreiben

        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko && !target.getErobertVon(own)) {
            target.erobern(own); //erobert das Land sofort, der ehemalige Besitzer des Landes hat das Spiel verloren.
        }
    }

    @Override
    public String toString() {
        String s1,s2,s3;
        if (own.equals(Land.spieler)) {
            s1 = "Ihr konnte eure ";
            s2 = "habt ";
            s3 = "euer";
        }
        else {
            s1 = "Der Oberste von "+own.getName()+"konnte seine ";
            s2 = "hat ";
            s3 = "sein";
        }

        switch (status) {
            case erfolg:
                ausgabe = s1+"Bevölkerung überzeugen und "+s2+gewinn+" Vertrauen erhalten";
                break;
            case fehlschlag:
                ausgabe = s1+"Bevölkerung nicht überzeugen, "+s3+" Standpunkt wurde ignoriert";
                break;
        }
        return ausgabe;
    }
}
