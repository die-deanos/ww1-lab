package objects.politicskills.diktatur;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Einparteisystem extends Passiv {

    private Skill parent2;

    public Einparteisystem(Land own, Skill parent, Skill parent2) {
        super(  "Einparteisystem",
                "Das Land wird von einer Partei regiert. Vertrauen zu anderen Nationen kann nicht unter 0% fallen.",
                own,
                parent,
                1,
                new int[]{200});

        this.parent2  = parent2;
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("EinHerrscherSystem")) {
                EinHerrscherSystem e = (EinHerrscherSystem) s;
                if(e.getFreigeschaltet()){
                    return false;
                }
            }
        }
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething() {

    }
}
