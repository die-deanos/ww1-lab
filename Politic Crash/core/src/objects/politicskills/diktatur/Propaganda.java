package objects.politicskills.diktatur;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Propaganda extends Passiv {
    public Propaganda(Land own) {
        super(  "Propaganda",
                "Die Medien bestimmen die Feindbilder. Der Vertrauenswert wird anderen Ländern falsch angezeigt.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Waffengewalt")) {
                Waffengewalt w = (Waffengewalt) s;
                if(w.getFreigeschaltet()) {
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {

    }
}
