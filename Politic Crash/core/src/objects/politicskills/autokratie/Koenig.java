package objects.politicskills.autokratie;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Koenig extends Passiv {
    public Koenig(Land own) {
        super(  "König",
                "Bestimmt einen normalen Typen zum Landeschef. Gewährt ründlich eine Chance Bonus Vertrauen zu generieren.", //chance = 15%, Bonusvertrauen = 10
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Papst")) {
                Papst e = (Papst) s;
                if(e.getFreigeschaltet()) {
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {

    }
}
