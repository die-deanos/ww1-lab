package objects.politicskills.autokratie;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;


public class Papst extends Passiv {
    public Papst(Land own) {
        super(  "Papst",
                "Bestimmt das kirliche Oberhaupt zum Landeschef. Erhöht ründlich das Vertrauen zu anderen Ländern um 2.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("König")) {
                Koenig st = (Koenig) s;
                if(st.getFreigeschaltet()) {
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {

    }
}
