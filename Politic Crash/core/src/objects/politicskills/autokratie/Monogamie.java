package objects.politicskills.autokratie;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Monogamie extends Passiv {

    private Skill parent2;

    public Monogamie(Land own, Skill parent, Skill parent2) {
        super(  "Monogamie",
                "Erlaubt Eheschließungen mit nur einem Partner. Gewährt einen Vertrauensbonus pro Runde.",
                own,
                parent,
                1,
                new int[]{200});

        this.parent2  = parent2;
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Polygamie")) {
                Polygamie p = (Polygamie) s;
                if(p.getFreigeschaltet()){
                    return false;
                }
            }
        }
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething() {
        own.vertrauenAendern(3,own);
    }
}
