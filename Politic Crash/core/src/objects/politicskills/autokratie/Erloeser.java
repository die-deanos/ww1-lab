package objects.politicskills.autokratie;

import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Erloeser extends Aktiv {

    private Skill parent2;

    public Erloeser(Land own, Skill parent, Skill parent2) {
        super(  "Mesias",
                "Versucht ein Land zu übernehmen, ist unabhängig vom Vertrauen der Bevölkerung.",
                own,
                parent,
                1,
                new int[]{500},
                80,
                5,
                0,
                false);

        this.parent2 = parent2;
    }

    @Override
    public boolean unlock() {
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething(Land target) {
        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko && !target.getErobertVon(own)) {
            target.erobern(own); //erobert das Land sofort, der ehemalige Besitzer des Landes hat das Spiel verloren.

        }
    }

    @Override
    public String toString() {
        String s1;
        if (own.equals(Land.spieler)) s1 = "";
        else s1 = " aus "+own.getName();

        switch (status) {
            case erfolg:
                ausgabe = "Der Erlöser"+s1+" konnte "+target.getName()+" bekehren.";
                break;
            case fehlschlag:
                ausgabe = "Der Erlöser"+s1+" scheiterte bei seinem Versuch "+target.getName()+" zu bekehten.";;
                break;
        }

        return ausgabe;
    }
}
