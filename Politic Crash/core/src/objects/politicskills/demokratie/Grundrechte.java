package objects.politicskills.demokratie;

import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Grundrechte extends Aktiv {

    private Skill parent2;

    public Grundrechte(Land own, Skill parent, Skill parent2) {
        super(  "Demokratie",
                "Verspricht das Einhalten der Grundrechte. Vertrauen kann 3 Runden lang nicht in den Minusbereich sinken.",
                own,
                parent,
                1,
                new int[]{200},
                100,
                3,
                0,
                true);

        this.parent2 = parent2;
    }

    @Override
    public boolean unlock() {
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething(Land target) {
        int chance = lvl * 10 + (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko && !target.getErobertVon(own)) {
            target.erobern(own); //erobert das Land sofort, der ehemalige Besitzer des Landes hat das Spiel verloren.
        }
    }

    @Override
    public String toString() {
        String s1;
        if (own.equals(Land.spieler)) s1 = "Ihr versprecht";
        else s1 = "Die Regierung von "+own.getName()+" verspricht";

        switch (status) {
            case erfolg:
                ausgabe = s1+" das Einhalten der Grundrechte, die Bevölkerung ist zufrieden";
                break;
            case fehlschlag:
                ausgabe = s1+" das Einhalten der Grundrechte, die Bevölkerung ist jedoch misstrauisch";
                break;
        }
        return ausgabe;
    }
}
