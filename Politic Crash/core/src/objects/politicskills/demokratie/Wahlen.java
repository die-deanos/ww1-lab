package objects.politicskills.demokratie;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Wahlen extends Passiv {
    public Wahlen(Land own) {
        super(  "Wahlen",
                "Die Bevölkerung wählt absofort die Regierung. Vertrauen zum eigenen Land kann nicht mehr in den negativen Bereich fallen.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Volksentscheid")) {
                Volksentscheid v = (Volksentscheid) s;
                if(v.getFreigeschaltet()) {
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {

    }
}
