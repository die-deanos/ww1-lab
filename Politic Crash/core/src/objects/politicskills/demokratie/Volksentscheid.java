package objects.politicskills.demokratie;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Volksentscheid extends Passiv {
    public Volksentscheid(Land own) {
        super(  "Volksentscheid",
                "Die Bevölerung darf bei Gesetzen mit entscheiden. Das Maximum-Vertrauen ist nun bei 120%.",
                own,
                null,
                1,
                new int[]{200});
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Wahlen")) {
                Wahlen w = (Wahlen) s;
                if(w.getFreigeschaltet()) {
                    return false;
                }
            }
        }
        return super.unlock();
    }

    @Override
    public void doSomething() {

    }
}
