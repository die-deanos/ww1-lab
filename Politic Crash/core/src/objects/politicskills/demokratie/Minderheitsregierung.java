package objects.politicskills.demokratie;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Minderheitsregierung extends Passiv {

    private Skill parent2;

    public Minderheitsregierung(Land own, Skill parent, Skill parent2) {
        super(  "Minderheitsregierung",
                "Senkt die Freischaltekosten aller technischen Skills um 20.",
                own,
                parent,
                1,
                new int[]{200});

        this.parent2  = parent2;
    }

    @Override
    public boolean unlock() {
        for (Skill s : own.getSkillbaum().getPolitisch()) {
            if(s.getName().equals("Merkeln")) {
                Merkeln m = (Merkeln) s;
                if(m.getFreigeschaltet()){
                    return false;
                }
            }
        }
        if((parent.getFreigeschaltet() || parent2.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
            own.informationenAendern(-kosten[lvl], own);
            lvl=1;
            return true;
        }
        return false;
    }

    @Override
    public void doSomething() {

    }
}
