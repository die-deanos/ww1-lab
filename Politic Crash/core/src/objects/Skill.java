package objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

public class Skill {

	//Funktionsvariablen
	protected String name, beschreibung; //Name und in Worten beschriebene Funktion des Skills.
	protected Land own; //Zugehörigkeit Land.
	protected Skill parent; //Variable, welche Notwendig ist um den Skill freizuschalten.
	protected int[] kosten; //Kosten für Freischaltung und LevelUp. Einige Skills müssen nicht freigeschalten werden, da zählen kosten als Einsatzkosten.
	protected int lvl; //Die Stufe des Skills.
	protected int maxlvl; //Die maximale Stufe eines Skills.
	protected String ausgabe = ""; //Im Spiel sichtbarer Effekt des Skills.
	protected boolean passiv; //Ob Skill passiv

	//Anzeigevariablen
	protected Sprite[] sprites; //Positionen des Skills auf dem Screen.


	public Skill(String name, String beschreibung, Land own, Skill parent, int maxlvl, int[] kosten) { //Übergabe der bereits erforschten, notwendigen Fähigkeit
		this.name = name;
		this.beschreibung = beschreibung;
		this.own = own;
		this.parent = parent;
		this.lvl = 0; //Instanziierung des lvl´s, 0 bedeutet 'noch nicht freigeschalten'.
		this.maxlvl = maxlvl;
		this.kosten = kosten;

		//Spriteliste für alle Sprites mit Koordinaten aus SkillKoordinaten
		sprites = new Sprite[maxlvl];
		for(int c=1; c<=maxlvl; c++) {
			//Anderer Pfad für Transparente Symbole
			Sprite sprite;
			try {
				//Erstellt Sprite abhängig von Name
				sprite = new Sprite(new Texture("InGame\\Skilltree\\Symbole\\"+name+""+c+".png"));
			}
			catch (Exception e) { //Sondefall wenn nicht vorhanden
				System.out.println("Name: "+name+""+c);
				sprite = new Sprite(new Texture("InGame\\Skilltree\\Symbole\\Placeholder1.png"));
			}
			//Positioniert Sprite abhängig von Name
			sprite.setPosition(SkillKoordinaten.getX(name, c),SkillKoordinaten.getY(name, c));
			sprite.setSize(sprite.getWidth(), sprite.getHeight());
			sprites[c-1]=sprite;
		}
	}

	public boolean unlock() { //Methode zum Freischalten des Skills.
		//parent-Abfrage: Überprüfung ob der Skill einen zum-Freischalten-benötigen-Skill benötigt oder ob es der 1. seiner Art ist.
		//own-Abfrage: Überprüfung ob das Land genügend Informationen besitzt um diese in den Skill zu investieren.
		if((parent == null || parent.getFreigeschaltet()) && (own.getInformationen(own) >= kosten[lvl])) {
			own.informationenAendern(-kosten[lvl], own); //Abziehen der Kosten vom Informationspull
			lvl=1; //lvl 1 bedeutet, der Skill ist freigeschalten und kann eingesetzt werden.
			return true; //Skill ist freigeschaltet.
		}
		return false; //Skill ist nicht freigeschaltet.
	}

	public boolean lvlup() { //Methode zum Verbessern des Skills.
		//lvl-Abfrage: Überprüfung ob der Skill schon freigeschaltet ist und nicht sein maxlvl erreicht hat.
		//own-Abfrage: selbe wie bei unlock()
		if((lvl!=0) && (lvl != maxlvl) && (own.getInformationen(own) >= kosten[lvl])) {
			own.informationenAendern(-kosten[lvl], own);
			lvl++; //Inkrementiert das lvl um 1.
			return true; //Stufe des Skills wird um 1 erhöht.
		}
		else return false; //Nichts passiert.
	}

	public String toString() { //Gibt dem Spieler auskunft darüber was der Skill nach aktivierung getan hat.
		return ausgabe;
	}

	public String getName() { //Gibt den Namen des Skills zurück. Notwendig um auf den Skill von anderen Skills zuzugreifen, falls benötigt.
		return name;
	}

	public String getBeschreibung() { //Gibt die Beschreibung des Skills zurück. Notwendig für das hovern mit dem Mauszeiger über den Skill-Button.
		return beschreibung;
	}

	public int getLvl() { //Gibt das derzeitige lvl des Skills zurück. Abfrage für die lvl-up Buttons.
		return lvl;
	}

	public int getMaxlvl() {
		return maxlvl;
	}

	public boolean getFreigeschaltet() { //Gibt zurück, ob der Skill bereits freigeschaltet wurde. Notwendig für diverse Überprüfungen.
		if(lvl!=0) return true;
		else return false;
	}
	public int getKosten() { //Gibt die Kosten des Skills zurück. Notwendig für einige Freischalte- und lvlup- Abfragen.
		if(lvl>=kosten.length) return 100000; //Für Botabfrage, da bei dieser nicht verhindert wird das Skills auf dem maxlevel nicht weiter gelevelt werden können
		return kosten[lvl];
	}

	public boolean getPassiv() {
		return passiv;
	}

	public Sprite[] getSprites() { //Gibt die Sprite zurück. Notwendig für das UI.
		return sprites;
	}
}
