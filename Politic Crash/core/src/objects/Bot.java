package objects;

import com.mygdx.game.PoliticCrash;
import screens.InGameScreen;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Bot {

    int chanceAgressive, chanceNeutral, chanceFreundlich;

    private InGameScreen inGame;
    private ArrayList<Aktiv> aktiveSkills;
    private Land land;

    private ArrayList<Skill> passivAgressiv = new ArrayList<>();
    private ArrayList<Skill> passivNeutral = new ArrayList<>();
    private ArrayList<Skill> passivFreundlich = new ArrayList<>();
    private ArrayList<Aktiv> aktivAgressiv = new ArrayList<>();
    private ArrayList<Aktiv> aktivNeutral = new ArrayList<>();
    private ArrayList<Aktiv> aktivFreundlich = new ArrayList<>();

    public Bot(PoliticCrash.Gesinnung b, PoliticCrash game, Land land) {
        switch (b) {
            case agressiv:
                chanceAgressive = 5;
                chanceNeutral = 3;
                chanceFreundlich = 2;
                break;
            case neutral:
                chanceAgressive = 3;
                chanceNeutral = 4;
                chanceFreundlich = 3;
                break;
            case freundlich:
                chanceAgressive = 2;
                chanceNeutral = 3;
                chanceFreundlich = 5;
                break;
        }
        this.land=land;
        inGame=game.getInGame();
        aktiveSkills = land.getAktiveSkills();
    }

    public void botZug() {
        int zahl = (int) (Math.random() * 10);
        arraysFüllen(land);
        skillAussuchen(zahl, land);
    }

    public void arraysFüllen(Land land) {
        for (Skill s : land.getSkillbaum().getAgressiv()) {
            if (!aktiveSkills.contains(s) || s.getFreigeschaltet()) {
                if (s.getKosten() <= land.getInformationen(land)) {
                    if (land.getSkillbaum().getAktiv().contains(s)) {
                        aktivAgressiv.add((Aktiv) s);
                    } else {
                        passivAgressiv.add(s);
                    }
                }
            }
        }
        for (Skill s : land.getSkillbaum().getNeutral()) {
            if (!aktiveSkills.contains(s) || s.getFreigeschaltet()) {
                if (s.getKosten() <= land.getInformationen(land)) {
                    if (land.getSkillbaum().getAktiv().contains(s)) {
                        aktivNeutral.add((Aktiv) s);
                    } else {
                        passivNeutral.add(s);
                    }
                }
            }
        }
        for (Skill s : land.getSkillbaum().getFreundlich()) {
            if (!aktiveSkills.contains(s) || s.getFreigeschaltet()) {
                if (s.getKosten() <= land.getInformationen(land)) {
                    if (land.getSkillbaum().getAktiv().contains(s)) {
                        aktivFreundlich.add((Aktiv) s);
                    } else {
                        passivFreundlich.add(s);
                    }
                }
            }
        }
    }

    public void skillAussuchen(int zahl, Land land){
        int zufallsZahlAktiv = 0;
        int zufallsZahlPassiv = 0;

        if(zahl < chanceAgressive){
            if(!aktivAgressiv.isEmpty()) {
                //Aktive Skills
                zufallsZahlAktiv = (int) (Math.random() * aktivAgressiv.size());
                Aktiv skillAktiv = aktivAgressiv.get(zufallsZahlAktiv);

                land.levelSkill(skillAktiv);

                if(skillAktiv.eigen == true){
                    land.initiateSkill(skillAktiv, land);
                }else{
                    land.initiateSkill(skillAktiv, landAussuchen());
                }
            }
            //Passive Skills
            if(!passivAgressiv.isEmpty()) {
                zufallsZahlPassiv = (int) (Math.random() * passivAgressiv.size());

                Skill skillPassiv = passivAgressiv.get(zufallsZahlPassiv);
                land.levelSkill(skillPassiv);
            }

        }else if(zahl < chanceAgressive+chanceNeutral){
            if(!aktivNeutral.isEmpty()) {
                //Aktive Skills
                zufallsZahlAktiv = (int) (Math.random() * aktivNeutral.size());
                Aktiv skillAktiv = aktivNeutral.get(zufallsZahlAktiv);

                land.levelSkill(skillAktiv);

                if(skillAktiv.eigen == true){
                    land.initiateSkill(skillAktiv,land);
                }else{
                    land.initiateSkill(skillAktiv, landAussuchen());
                }
            }
            //Passive Skills
            if(!passivNeutral.isEmpty()) {
                zufallsZahlPassiv = (int) (Math.random() * passivNeutral.size());

                Skill skillPassiv = passivNeutral.get(zufallsZahlPassiv);
                land.levelSkill(skillPassiv);
            }

        }else{
            if(!aktivFreundlich.isEmpty()) {
                //Aktive Skills
                zufallsZahlAktiv = (int) (Math.random() * aktivFreundlich.size());
                Aktiv skillAktiv = aktivFreundlich.get(zufallsZahlAktiv);

                land.levelSkill(skillAktiv);

                if(skillAktiv.eigen == true){
                    land.initiateSkill(skillAktiv,land);
                }else{
                    land.initiateSkill(skillAktiv, landAussuchen());
                }
            }
            //Passive Skills
            if(!passivFreundlich.isEmpty()) {
                zufallsZahlPassiv = (int) (Math.random() * passivFreundlich.size());

                Skill skillPassiv = passivFreundlich.get(zufallsZahlPassiv);
                land.levelSkill(skillPassiv);
            }
        }
    }

    public Land landAussuchen(){
        int landZahl = (int) (Math.random()*8);
        return Land.values()[landZahl];
    }

}
