package objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.PoliticCrash;
import com.mygdx.game.PoliticCrash.Regierungsform;
import com.mygdx.game.PoliticCrash.Gesinnung;

import screens.MenuLandScreen;

import java.util.ArrayList;

public enum Land {

	spieler		("Spielerland", new Color(1,1,1,1)),
	deanotopia	("Deanotopia", new Color(133f/255f,197f/255f,126f/255f,1)),
	elisiana	("Elsiana",  new Color(236f/255f,167f/255f,0,1)),
	jesconia	("Jesconia",  new Color(19f/255f,187f/255f,181f/255f,1)),
	jojonien	("Jojonien", new Color(7f/255f,22f/255f,19f/255f,1)),
	pammelton	("Pammelton", new Color(245f/255f,201f/255f,250f/255f,1)),
	shayanistan	("Shayanistan", new Color(194f/255f, 53f/255f, 45f/255f,1)),
	stefanien	("Stefanien",  new Color(254f/255f,226f/255f,122f/255f,1));

	private String name; //Name des Landes
	private Color farbe; //Farbe des Overlays, Farbe für Colorpicking System

	private int[] vertrauen = new int[]{20, 20, 20, 20, 20, 20, 20, 20}; //Vertrauen zu den anderen Ländern, Index in Relation zu Position des Landes in Land.values()
	private int[] informationen = new int[]{50, 50, 50, 50, 50, 50, 50, 50}; //Information zu den anderen Ländern, wie Vertrauen
	private boolean vertrauenVerloren = false;; //Loosing condition
	private Land erobertVon;
	private double[] bonimali = new double[]{1,1,1,1}; //Multiplikationen für Vertrauen / Informationen, durch Skills verändert
	private Texture overlay; //Texture der Overlays, welche das Land highlighted

	private MenuLandScreen screen; //Spezifischer Screen des Landes
	private Bot bot; //Botgegner welcher das Land kontroliert, wenn das Land noch nicht erobert ist

	//Skills
	private Skillbaum skillbaum; //Skillbaum mit allen Skills, welche freigeschaltet werden können
	private ArrayList<Aktiv> aktiveSkills = new ArrayList<Aktiv>(); //Liste der Skills, welche aktuell ausgeführt werden
	private int maxAktionen=2, anzahlAktionen=0; //Anzahl der Erlaubten aktionen in dieser Runde, aktuelle anzahl der ausgeführten Aktionen
	private Skill[] aktionen = new Skill[maxAktionen]; //Speichern der Skills welche aktiviert wurden

	/** Konstruktor */
	Land(String name, Color farbe) { //Konstruktor
		this.name = name;
		this.farbe = farbe;
		erobertVon = this;

		overlay = new Texture("InGame\\Main\\Overlay"+name+".png");
	}

	//Teil des instanziieren, muss extra stehen, da hier das Land sich selbst übergibt

	/** Methode um alle Variablen der Botländer zu instanziieren, welche das eigene Land als Variable brauchen
	 * @param game		Spiel, zum wechseln des Screens, für MenuLandScreen
	 * @param batch		zum Zeichnen der Grafik, für MenuLandScreen */
	public void setBotLinks(PoliticCrash game, SpriteBatch batch) {

		if(this != spieler) {

			this.screen = new MenuLandScreen(game, batch, this); //Screens instanziieren

			//Skillbaum mit Regierungsform instanziieren
			Regierungsform r;
			if (this==deanotopia || this==pammelton) r= Regierungsform.diktatur;
			else if (this==elisiana || this==jesconia) r= Regierungsform.demokratie;
			else if (this==jojonien || this==stefanien) r= Regierungsform.autokratie;
			else r= Regierungsform.kommunismus;
			this.skillbaum = new Skillbaum(r, this);

			//Bots instanziieren
			Gesinnung g;
			if (this==deanotopia || this==pammelton || this==jojonien) g= Gesinnung.agressiv;
			else if (this==stefanien || this==jesconia) g= Gesinnung.neutral;
			else g= Gesinnung.freundlich;
			bot = new Bot(g, game,this);

		}
	}

	/** Methode um alle Variablen der Botländer zu instanziieren, welche das eigene Land als Variable brauchen
	 * @param game				Spiel, zum wechseln des Screens, für MenuLandScreen
	 * @param batch				zum Zeichnen der Grafik, für MenuLandScreen
	 * @param name				Name welchen der Spieler gewählt hat
	 * @param farbe				Farbe welcher der Spieler als Hintergrund gewählt hat
	 * @param regierungsform	Regierungsform welche der Spieler für sein Land bestimmt hat */
	public void setPlayerLinks(PoliticCrash game, SpriteBatch batch, String name, Color farbe, Regierungsform regierungsform) {
		if(this == spieler) {
			this.name = name;
			this.farbe = farbe;
			this.screen = new MenuLandScreen(game, batch, this);
			this.skillbaum = new Skillbaum(regierungsform, this);
		}
	}

	//Vertrauen und Informationen immer in Bezug auf ein Land

	public int getVertrauen(Land land) {
		return vertrauen[findIndex(land)];
	}

	public int getInformationen(Land land) {
		return informationen[findIndex(land)];
	}

	public void vertrauenAendern(int menge, Land land) {

		if (land.equals(this)) { //Für eigenes Land mit Boni/Mali und Abfrage für Filter

			boolean filter = false;

			//Suchalgoythmus, überprüft im Land ob der Skill Filter aktiviert wurde. (linear search)
			for (Aktiv a : skillbaum.getAktiv()) {
				if (a.getName().equals("Filter")) {
					filter = a.getImEinsatz();
				}
			}
			if (!filter) { //Wenn Filter nicht aktiv ist, so kann das Vertrauen normal geändert werden.
				if (menge > 0) vertrauen[findIndex(land)] += (int) (menge * bonimali[0]);
				else vertrauen[findIndex(land)] += (int) (menge * bonimali[1]);
			}
			else if (menge > 0 && land.equals(this)) { //Wenn Filter aktiv ist, so kann das Vertrauen nur im positiven Zahlenbereich beeinflusst werden.
				vertrauen[findIndex(land)] += (int) (menge * bonimali[0]);
			}
		}
		else vertrauen[findIndex(land)] += menge;
	}

	public void informationenAendern(int menge, Land land) {
		if(land.equals(this)) { //Mit Boni / Mali
			if(menge > 0) informationen[findIndex(land)] += (int) (menge * bonimali[2]);
			else informationen[findIndex(land)] += (int) (menge * bonimali[3]);
		} else informationen[findIndex(land)] += menge; //normal für alle Anderen
	}

	//Algorythmus zum Finden des Index eines Landes (linear Search)
	private int findIndex(Land land) {
		int index=-1;
		for (int i = 0; i < Land.values().length; i++) {
			if (land.equals(Land.values()[i])) {
				index = i;
				break;
			}
		}
		return index;
	}

	//Getter/Setter für Variablen

	public String getName() {
		return name;
	}
	
	public Color getFarbe() {
		return farbe;
	}
	
	public MenuLandScreen getScreen() {
		return screen;
	}
	
	public Skillbaum getSkillbaum() {
		return skillbaum;
	}

	public boolean getVertrauenVerloren() {
		return vertrauenVerloren;
	}

	public boolean getErobertVon(Land l) {
		if (erobertVon.equals(l)) return true;
		else return false;
	}

	public Land getErobertVon() {
		return erobertVon;
	}

	public void erobern(Land l) {
		erobertVon = l;
	}

	public double[] getBonimali() {
		return bonimali;
	}

	public void setBonimali(double boni1, double mali1, double boni2, double mali2) {
		this.bonimali = new double[]{boni1, mali1, boni2, mali2};
	}

	public Texture getOverlay() {
		return overlay;
	}

	public Bot getBot() {
		return bot;
	}

	//Skill aktivierung

	/** Aufruf am Ende der Runde, ruft alle freigeschalteten passiven und aktiven Skills in Aktion auf
	 *  resettet desweiteren die übrigen Aktionen pro runde */
	public void skillAufruf() {
		//ruft alle aktiven Skills der Liste auf
		for (int i=0; i<aktiveSkills.size(); i++) {
			if(!aktiveSkills.get(i).runde()) { //skill speichert zielland, muss nicht erneut übergeben werden
				aktiveSkills.remove(aktiveSkills.get(i)); //Objekt entfernen
				i--; //Pointer neu ausrichten
			}
		}
		//ruft alle passiven Skills der Liste auf
		for (Passiv p : skillbaum.getPassiv()) {
			if (p.getFreigeschaltet()) p.doSomething(); //methode welche den effekt des Skills enthällt
		}

		//Anfang der Runde => Aktionen resetten
		for(int i=0; i<aktionen.length; i++) {
			aktionen[i] = null;
		}
		anzahlAktionen=0;
	}

	public ArrayList<Aktiv> getAktiveSkills() {
		return aktiveSkills;
	}

	public Skill[] getAktionen() {
		return aktionen;
	}

	/**	Schaltet frei / levelt einen Skill, sofern Möglich
	 * @param skill		der freizuschaltende / zu levelnde Skill */
	public void levelSkill(Skill skill) {
		if(anzahlAktionen<maxAktionen) {
			if (!skill.getFreigeschaltet()) {
				if (skill.unlock()) {
					aktionen[anzahlAktionen] = skill;
					anzahlAktionen++;
				}
			}
			else {
				if (skill.lvlup()) {
					aktionen[anzahlAktionen] = skill;
					anzahlAktionen++;
				}
			}
		}
	}

	/**	aktiviert einen Skill sofern möglich
	 * @param aktiv		der zu aktivierende Skill
	 * @param target	das Ziel des Skills */
	public void initiateSkill(Aktiv aktiv, Land target) {
		if (anzahlAktionen<maxAktionen) {
			if(!aktiveSkills.contains(aktiv)) {
				if(aktiv.getAnwendungsKosten()<=informationen[findIndex(target)]) {
					aktiv.initialisieren(target);
					aktionen[anzahlAktionen] = aktiv;
					anzahlAktionen++;
					if(aktiv.getMaxRunde()>0 || aktiv.getDauer()>0) aktiveSkills.add(aktiv);
				}
			}
		}
	}


}
