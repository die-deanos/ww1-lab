package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Funkstelle extends Passiv {

    public Funkstelle(Land own) {
        super(  "Funkstelle",
                "Schaltet Funkstellen Technologie frei.",
                own,
                null,
                1,
                new int[]{80}
        );
    }

    @Override
    public boolean lvlup() { //kann nicht gelevelt werden
        return false;
    }
}
