package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Inlandsnachrichten extends Passiv {

    public Inlandsnachrichten(Land own, Skill parent) {
        super(  "Inlandsnachrichten",
                "Schaltet News aus dem eigenen Land frei.",
                own,
                parent,
                3,
                new int[]{50, 100, 200} //Stefan kosten anpassen
        );
    }
}
