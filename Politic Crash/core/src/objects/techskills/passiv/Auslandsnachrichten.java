package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Auslandsnachrichten extends Passiv {
    public Auslandsnachrichten(Land own, Skill parent) {
        super(  "Auslandsnachrichten",
                "Schaltet globale News frei.",
                own,
                parent,
                4,
                new int[]{50, 100, 200, 300} //Stefan kosten anpassen
        );
    }
}
