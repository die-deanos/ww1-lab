package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class FreiesInternet extends Passiv{

    public FreiesInternet(Land own, Skill parent) {
        super(  "Freies Internet",
                "Schaltet den weltweiten Internetzugung für die Bevölkerung frei, ohne Filter. Erhöht das Vertrauen der Bevölkerung pro Runde",
                own,
                parent,
                1,
                new int[]{100}
        );
    }

    @Override
    public boolean lvlup() { //kann nicht gelevelt werden
        return false;
    }

    @Override
    public void doSomething() {
        int passiv = (int) (Math.random() * 5 + 1); //zufälliger Vertrauenswert.
        own.vertrauenAendern(passiv, own); //passives Vertrauenseinkommen.
    }
}
