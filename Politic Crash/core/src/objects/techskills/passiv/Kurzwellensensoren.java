package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Kurzwellensensoren extends Passiv {

    public Kurzwellensensoren(Land own, Skill parent) {
        super(  "Kurzwellensensoren",
                "Lässt die Bevölkerung Radio hören. Steigert das Vertrauen pro Runde.",
                own,
                parent,
                2,
                new int[]{40, 100}
        );
    }

    @Override
    public void doSomething() {
        int passivesEinkommen = (int) (Math.random() * lvl * 5 + 1); //zufälliger Vertrauenswert.
        own.vertrauenAendern(passivesEinkommen, own); //passives Vertrauenseinkommen.
    }
}
