package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Pc extends Passiv{

    public Pc(Land own) {
        super(  "PC",
                "Schaltet PC-Technologien frei.",
                own,
                null,
                1,
                new int[]{70}
                );
    }

    @Override
    public boolean lvlup() { //kann nicht gelevelt werden
        return false;
    }
}
