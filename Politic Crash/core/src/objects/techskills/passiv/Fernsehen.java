package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Fernsehen extends Passiv {

    public Fernsehen(Land own) {
        super(  "Fernsehen",
                "Erhöht das Vertrauen der Bevölkerung",
                own,
                null,
                1,
                new int[]{150}
        );
    }

    @Override
    public boolean lvlup() { //kann nicht gelevelt werden
        return false;
    }
}
