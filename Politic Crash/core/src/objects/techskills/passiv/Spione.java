package objects.techskills.passiv;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import objects.Land;
import objects.Passiv;
import objects.Skill;

import java.util.ArrayList;

public class Spione extends Passiv {

    public Spione(Land own) {
        super(  "Spione",
                "Schaltet Spione frei.",
                own,
                null,
                1,
                new int[]{100}
        );
    }

    @Override
    public boolean lvlup() { //kann nicht gelevelt werden
        return false;
    }
}
