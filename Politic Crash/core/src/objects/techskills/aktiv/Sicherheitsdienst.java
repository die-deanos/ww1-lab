package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Sicherheitsdienst extends Aktiv {
    public Sicherheitsdienst(Land own, Skill parent) {
        super(  "Sicherheitsdienst",
                "Chance auf Spionageabwehr.",
                own,
                parent,
                2,
                new int[]{90,0}, //@Stefan Kosten überarbeiten
                40,
                0,
                2,
                true);
    }

    @Override
    public void doSomething(Land target) {
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if (chance >= risiko) { //Bei 1. Aktivierung ist sdienst = false
            status= PoliticCrash.StatusRunde.erfolg; //Sobald auf true gesetzt wird beginnt die eig. Rundendauer.
        } else {
            status= PoliticCrash.StatusRunde.fehlschlag; //Bei Beendigung des Skills wird sdienst wieder auf false gesetzt und der Skill endet.
        }
    }

    @Override
    public String toString() {
        String s1, s2, s3;
        if (own.equals(Land.spieler)) s1 = "";
        else s1 = " von "+own.getName();

        switch (status) {
            case erfolg:
                ausgabe = "Der Sicherheitsdienst"+s1+" hat nun ein waches Auge.";
                break;
            case fehlschlag:
                ausgabe = "Der Sicherheitsdienst"+s1+" ist zu betrunken um aufzupassen.";
                break;
        }
        return ausgabe; //Gibt entsprechende Ausgabe zurück.
    }
}
