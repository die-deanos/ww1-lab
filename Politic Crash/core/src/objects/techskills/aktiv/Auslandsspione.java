package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Auslandsspione extends Aktiv {

    private double gewinn; //Variable für den positiven Effekt des Skills.
    private double verlust; //Variable für den negativen Effekt des Skills.
    private int ausgabeNr; //Erweiterung für Ausgabe.

    public Auslandsspione(Land own, Skill parent) {
        super(  "Auslandsspione",
                "Eure Spione versuchen das Land zu infiltrieren.",
                own,
                parent,
                3,
                new int[]{70, 120, 200},
                70,
                2,
                0,
                false);

        gewinn = 15;
        verlust = 20;
    }

    @Override
    public boolean lvlup() {
        if (super.lvlup()) { //Variablen anpassen bei lvl up.
            risiko =- lvl * 10;
            gewinn =+ lvl * 2.5;
            verlust =+ lvl * 1.5;
            return true;
        }
        else return false;
    }

    @Override
    public void doSomething(Land target) {
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat. Mit lvl-abhängiger Mindesterfolgschance.
        boolean sicherheit = false; //Zwischenvariable, um den aktuellen Status im target abzufragen.

        //Suchalgoythmus, überprüft im target ob der Skill Sicherheitsdienst aktiviert wurde.
        for (Aktiv a : target.getSkillbaum().getAktiv()) {
            if(a.getName().equals("Sicherheitsdienst")) {
                sicherheit = a.getImEinsatz();
                break;
            }
        }

        //sicherheit gibt an, ob der Skill überhaupt Erfolg haben kann. true = Skill schlägt fehl, false = Skill kann funktionieren.
        if (!sicherheit) {
            //chance >= risiko gibt an, ob der Skill Erfolg hat.
            if (chance >= risiko) {
                own.informationenAendern((int) gewinn, target); //Wirkung des Skills bei Erfolg.
                status = PoliticCrash.StatusRunde.erfolg;
            } else {
                target.vertrauenAendern((int) -verlust, own); //Wirkung des Skills bei Misserfolg.
                status = PoliticCrash.StatusRunde.fehlschlag;
                ausgabeNr = 1;
            }
        } else {
            status = PoliticCrash.StatusRunde.fehlschlag;
            ausgabeNr = 2;
        }
    }

    @Override
    public String toString() {
        String s1, s2, s3;
        if (own.equals(Land.spieler)) {
            s1 = "Eure Spione";
            s2 = "Ihr";
            s3 = "eure Spione";
        }
        else {
            s1 = "Spione aus "+own.getName();
            s2 = own.getName();
            s3 = "Spione aus "+own.getName();
        }

        switch (status) {
            case erfolg:
                ausgabe = s1+" konnte " + target.getName() + " infiltrieren und " + gewinn + " Informationen beschaffen.";
                break;
            case fehlschlag:
                switch(ausgabeNr) {
                    case 1:
                        ausgabe = s1+" haben versagt! "+s2+" verliert " + verlust + " Vertrauen zu " + target.getName() + ".";
                        break;
                    case 2:
                        ausgabe = target.getName() + " hat seine Sicherheitskräfte mobilisiert, "+s3+" können nicht eindringen.";
                        break;
                }
        }
        return ausgabe; //gibt den entsprechenden Status/Sonderfall des Skills in Form eines Strings zurück. Dieser wird dann im UI ausgegeben.
    }
}
