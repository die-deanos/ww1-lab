package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Unterhaltungsprogramm extends Aktiv {

    private int gewinn; //Variable für den positiven Effekt des Skills.
    private int mindestVertrauen; //Variable für die Abfrage ob der Skill funktionieren kann.

    public Unterhaltungsprogramm(Land own, Skill parent) {
        super("Unterhaltungsprogramm",
                "Sendet eine satirische Sendung, erhöht jede Runde das Vertrauen um 7. Kann Fehlschlagen wenn das Vertrauen sehr niedrig ist",
                own,
                parent,
                2,
                new int[]{350,0}, //@Stefan fehlenden wert eintragen
                30,
                0,
                5,
                true
                );

        gewinn = 7;
    }

    @Override
    public boolean lvlup() {
        gewinn = 10;
        beschreibung = "Sendet eine satirische Sendung. Erhöht jede Runde das Vertrauen um 10.";
        return super.lvlup();
    }

    @Override
    public void doSomething(Land target) {
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.

        if(own.getVertrauen(target) <= 20) {
            own.vertrauenAendern(gewinn, target);
            status= PoliticCrash.StatusRunde.erfolg;
        } else {
            if(chance >= risiko) {
             own.vertrauenAendern(gewinn, target);
             status= PoliticCrash.StatusRunde.erfolg;
            }
        }
    }

    @Override
    public String toString() {
        String s1, s2, s3;
        if (own.equals(Land.spieler)) s1 = "";
        else s1 = " von "+own.getName();

        return ausgabe = "Die Sendung"+s1+" schlägt ein wie eine Bombe!"; //Gibt entsprechende Ausgabe zurück.
    }
}
