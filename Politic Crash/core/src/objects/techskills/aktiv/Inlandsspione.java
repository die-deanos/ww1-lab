package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Inlandsspione extends Aktiv {

    private int gewinn; //Variable für den positiven Effekt des Skills.
    private int verlust; //Variable für den negativen Effekt des Skills.

    public Inlandsspione(Land own, Skill parent) {
        super(  "Inlandsspione",
                "Spioniert die eigene Bevölkerung aus. Gewährt viel Datenmenge, besitzt aber auch ein hohes Risiko.",
                own,
                parent,
                3,
                new int[]{20, 50, 100},
                70,
                1,
                0,
                true
        );

        gewinn = 40;
        verlust = 35;

    }

    @Override
    public boolean lvlup() {
        gewinn += lvl * 10;
        verlust -= lvl * 5;
        risiko -= 5;
        return super.lvlup();
    }

    @Override
    public void doSomething(Land target) {
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if(chance >= risiko) { //chance >= risiko gibt an, ob der Skill Erfolg hat.
            status= PoliticCrash.StatusRunde.erfolg;
            own.informationenAendern(gewinn, target);
        } else {
            status= PoliticCrash.StatusRunde.fehlschlag;
            own.vertrauenAendern(-verlust, target);
        }
    }

    @Override
    public String toString() {
        String s1, s2, s3;
        if (own.equals(Land.spieler)) {
            s1 = "Ihre Spione";
            s2 = "eure";
            s3 = "Ihr habt ";
        }
        else {
            s1 = "Spione in "+own.getName();
            s2 = "die";
            s3 = own.getName()+" hat ";
        }

        switch (status) {
            case erfolg:
                ausgabe = s1 + " hatten Erfolg! Es konnten " + gewinn + " Informationen der Befölkerung beschaffen werden.";
                break;
            case fehlschlag:
                ausgabe = s1 + " sind aufgeflogen und " + s2 + " Bevölkerung ist enttäuscht! " + s3 + verlust + " Vertrauen verloren.";
                break;
        }
        return ausgabe; //Gibt entsprechende Ausgabe zurück.
    }
}
