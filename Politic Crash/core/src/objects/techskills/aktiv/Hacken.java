package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.*;

public class Hacken extends Aktiv{

    private int gewinn; //Variable für den positiven Effekt des Skills.
    private double verlust; //Variable für den negativen Effekt des Skills.
    private int ausgabeNr; //Erweiterung für Ausgabe.

    public Hacken(Land own, Skill pc) { //Initialisiert den Skill mit allen übergebenen Variablen.
        super(  "Hacken",
                "Versucht die Server eines anderen Landes zu hacken, gewährt bei Erfolg Datenmenge.",
                own,
                null,
                4,
                new int[]{80, 120, 200, 300},
                60,
                2,
                0,
                false
        );

        gewinn = 20;
        verlust = 25;
    }

    public boolean lvlup() {
        if (super.lvlup()) {
            gewinn *= lvl * 2;
            verlust -= lvl * 3.5;
            risiko -= lvl * 2;
            return true;
        }
        else return false;
    }

    @Override
    public void doSomething(Land target) { //Funktionsmethode: Beschreibt was der Skill zu tun hat.
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        boolean firewall=false;

        //Suchalgoythmus, überprüft im target ob der Skill Firewall aktiviert wurde.
        for (Aktiv a : target.getSkillbaum().getAktiv()) {
            if(a.getName().equals("Firewall")) {
                firewall = a.getImEinsatz();
                break;
            }
        }

        if (!firewall) {
            //chance >= risiko gibt an, ob der Skill Erfolg hat.
            if (chance >= risiko) {
                own.informationenAendern(gewinn, target); //Wirkung des Skills bei Erfolg.
                status = PoliticCrash.StatusRunde.erfolg;
            } else {
                target.vertrauenAendern((int) -verlust, own); //Wirkung des Skills bei Misserfolg.
                status = PoliticCrash.StatusRunde.fehlschlag;
                ausgabeNr = 1;
            }
        } else {
            status = PoliticCrash.StatusRunde.fehlschlag;
            ausgabeNr = 2;
        }
    }

    @Override
    public String toString() {
        String s1, s2, s3;
        if (own.equals(Land.spieler)) {
            s1 = "";
            s2 = "Ihr";
            s3 = "ihr seid ";
        }
        else {
            s1 = " von "+own.getName();
            s2 = own.getName();
            s3 = own.getName()+" ist ";
        }

        switch(status) {
            case erfolg:
                ausgabe = "Der Hackerangriff"+s1+" war erfolgreich, es konnten " + gewinn + " Daten von " + target + " beschafft werden.";
                break;
            case fehlschlag:
                switch(ausgabeNr) {
                    case 1:
                        ausgabe = "Der Hackerangriff"+s1+" ist aufgeflogen! "+s2+" verliert " + verlust + " Vertrauen zu " + target.getName() + ".";
                        break;
                    case 2:
                        ausgabe = target.getName() + " hat seine Firewall hochgefahren, "+s3+" aufgeflogen.";
                        break;
                }
        }
        return ausgabe; //gibt den entsprechenden Status/Sonderfall des Skills in Form eines Strings zurück. Dieser wird dann im UI ausgegeben.
    }
}
