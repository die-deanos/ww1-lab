package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Firewall extends Aktiv{
    public Firewall(Land own, Skill parent) { //Initialisiert den Skill mit allen übergebenen Variablen.
        super(  "Firewall",
                "Chance Hackerangriffe abzuwehren.",
                own,
                parent,
                3,
                new int[]{80, 140, 220},
                60,
                0,
                1,
                true
        );
    }

    @Override
    public boolean lvlup() {
        if (super.lvlup()) { //Variablen anpassen
            dauer = lvl;
            risiko = 70 -(lvl*10);
            beschreibung = "Chance, " + dauer + " Runden lang Hackerangriffe abzuwehren.";
            return true;
        }
        else return false;
    }

    @Override
    public void doSomething(Land target) {
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.

        if (chance >= risiko) {
            status=PoliticCrash.StatusRunde.erfolg;
        } else {
            status= PoliticCrash.StatusRunde.fehlschlag;
        }
    }

    @Override
    public String toString() {
        String s1;
        if (own.equals(Land.spieler)) s1 = "";
        else s1 = " von "+own.getName();

        switch(status) {
            case erfolg:
                ausgabe = "Die Firewall"+s1+" wurde erfolgreich hochgefahren.";
                break;
            case fehlschlag:
                ausgabe = "Unbekannter Fehler im System, die Firewall"+s1+" konnte nicht hochgefahren werden.";
                break;
        }
        return ausgabe;
    }
}
