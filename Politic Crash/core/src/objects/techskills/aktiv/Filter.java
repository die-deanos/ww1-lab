package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

public class Filter extends Aktiv {

    public Filter(Land own, Skill parent) {
        super("Nachrichten",
                "Filtert Informationen herraus welche dem Land schaden könnten. Verhindert Vertrauenseinbrüche.",
                own,
                parent,
                1,
                new int[]{300},
                30,
                0,
                2,
                true
        );
    }

    @Override
    public void doSomething(Land target) {
        int chance = (int) (Math.random() * 101 + 1); //Prozentuale Chance ob der Skill Erfolg hat.
        if (chance >= risiko) { //chance >= risiko gibt an, ob der Skill Erfolg hat.
            status = PoliticCrash.StatusRunde.erfolg;
        } else {
            status = PoliticCrash.StatusRunde.fehlschlag;
        }
    }

    @Override
    public String toString() {
        String s1, s2;
        if (own.equals(Land.spieler)) {
            s1 = "";
            s2 = "";
        }
        else {
            s1 = own.getName()+" hat seine ";
            s2 = " von "+own.getName();
        }

        switch (status) {
            case erfolg:
                ausgabe = s1+"Filter erfolgreich eingesetzt. Schädlichen Informationen werden herrausgefiltert.";
                break;
            case fehlschlag:
                ausgabe = "Uploadfehler. Filter"+s2+" konnten nicht implementiert werden.";
                break;
        }
        return ausgabe; //Gibt entsprechende Ausgabe zurück.
    }

}