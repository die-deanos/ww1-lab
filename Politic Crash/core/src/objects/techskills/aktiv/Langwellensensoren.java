package objects.techskills.aktiv;

import com.mygdx.game.PoliticCrash;
import objects.Aktiv;
import objects.Land;
import objects.Skill;

import java.util.ArrayList;

public class Langwellensensoren extends Aktiv {

    private ArrayList<Land> ziele = new ArrayList<Land>(); //Array Liste, welche alle möglichen Ziele zwischenspeichert.
    private int gewinn; //Variable für den positiven Effekt des Skills.

    public Langwellensensoren(Land own, Skill parent) {
        super(  "Langwellensensoren",
                "Sammelt zwischen 10 und 30 Daten aus einem zufälligen Land, welches ebensfalls Langwellensensoren besitzt.",
                own,
                parent,
                2,
                new int[]{100, 150},
                0,
                1,
                0,
                true
                );
    }

    @Override
    public boolean lvlup() {
        gewinn = (int) (Math.random() * 31 + 20);
        beschreibung = "Sammelt zwischen 20 und 50 Daten aus einem zufälligen Land, welches ebensfalls Langwellensensoren besitzt.";
        return super.lvlup();
    }

    @Override
    public void initialisieren(Land target) {
        runde = maxrunde+dauer;
        this.target = target;
        status = PoliticCrash.StatusRunde.warten;

        //Alle Länder mit Langwellentechnologie adden
        for (Land l : Land.values()) {
            boolean techVorhanden=false;
            for (Skill s : l.getSkillbaum().getAktiv()) {
                if(s.getName().equals("Langwellensensoren")) {
                    techVorhanden = s.getFreigeschaltet();
                    break;
                }
            }
            if (!l.equals(own) && techVorhanden) ziele.add(l);
        }
    }

    @Override
    public void doSomething(Land target) {
        gewinn = (int) (Math.random() * 21 + 10);
        status= PoliticCrash.StatusRunde.erfolg;
        own.informationenAendern(gewinn, target);
    }

    @Override
    public String toString() {
        String s1, s2, s3;
        if (own.equals(Land.spieler)) {
            s1 = "Es konnten ";
            s2 = " beschafft werden.";
            s3 = "Scheinbar gibt es noch kein anderes Land welche Langwellensensoren besitzt. Ihr konntet keine Daten beschaffen.";
        }
        else {
            s1 = own.getName()+" konnte ";
            s2 = " beschaffen.";
            s3 = own.getName()+ "ist das einzige Land mit Langwellensensoren. Es konnte keine Daten beschaffen";
        }
        switch (status) {
            case erfolg:
                ausgabe = s1 + gewinn + " Informationen aus " + target.getName() + s2;
                break;
            case fehlschlag:
                ausgabe = s3;
                break;
        }
        return ausgabe; //Gibt entsprechende Ausgabe zurück.
    }
}
