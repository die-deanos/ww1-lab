package objects;

import com.badlogic.gdx.graphics.Color;
import screens.InGameScreen;

import java.util.ArrayList;
import java.util.Random;

public class Nachrichtsystem {

    private static final ArrayList <Nachricht> nachrichten = new ArrayList<>(); // Liste der Nachrichten
    private static final ArrayList<String> quatsch = new ArrayList<>(); //Eine liste mit Quatschnachrichten

    private static final Color inLand = new Color(51f/255f,78f/255f,1f,1f);
    private static final Color ausLand = new Color(1f,50f/255f,50f/255f,1f);
    private static final Color spass = new Color(63f/255f,63f/255f,63f/255f,1f);
    private static final Color spezial = new Color(1f, 173f/255f, 22f/255f, 1f);

    private static Passiv inLandSkill, ausLandSkill;
    private static InGameScreen inGame;

    public static void setLinks(InGameScreen screen){
        //Skills herraussuchen
        for(Passiv p : Land.spieler.getSkillbaum().getPassiv()) {
            System.out.println(p.getName());
            if (p.getName().equals("Inlandsnachrichten")) inLandSkill =p;
            else if (p.getName().equals("Auslandsnachrichten")) ausLandSkill =p;
            if(inLandSkill !=null && ausLandSkill !=null) break;
        }
        //InGame verlinken (für amZug)
        inGame = screen;
    }

    public static void einlesenQuatsch(){ //Hier stehen alle quatschnachrichten
        quatsch.add("Trauriger Angeklgter Hacker klaut Lachgummi!");
        quatsch.add("Bambi bekommt neuverfilmung!");
        quatsch.add("Spüler explodiert wegen Hitze");
        quatsch.add("Jojo revidiert Zitat: „Hasse Golffahrer genauso sehr wie Golfer!“");
        quatsch.add("Dean gewinnt die US Open");
        quatsch.add("Jesco wird Country-Star");
        quatsch.add("Lösung für Längen-Debatte gefunden: Erste Ziehharmonika-Plätze in Shayanistan");
        quatsch.add("Geheimnis gelüftet: Elsia ist ein Cyborg!");
        quatsch.add("Kampf gegen Slow Play: Mann mit Stoppuhr und Trillerpfeifer verfolgt künftig die Pros auf Schritt und Tritt");
        quatsch.add("Pammelton testet Hole-in-One-Raketenzündung");
        quatsch.add("Disney verlangt rechte an Stefanien");
        quatsch.add("Deanotopia von Exenmenschen übernommen?");
        quatsch.add("Elisiana sichtet Aliens!");
        quatsch.add("Jesconia investiert in Eis mit Biergeschmack");
        quatsch.add("So starb Knut der Eisbär ... ");
        quatsch.add("Jojonien kämpft für weltweite Hackfleischreduzierung!");
        quatsch.add("Banana Joe drückt den Bananenpreis runter auf zwei Pesus");
        quatsch.add("Luffy findet das One Piece");
        quatsch.add("HSMW Team gewinnt Worlds");
        quatsch.add("Alexander van Marbach züchtet Superchilischoten mit 5 Mio Scoville");
        quatsch.add("Weltweite Pentapocken Pandemie");
        quatsch.add("Blinder Bandit gewinnt Worldcup in Kung Fu");
        quatsch.add("Neue Hitsingel veröffentlicht: Toss a Coin to your Witcher von Jaskier");
        quatsch.add("Deanotopia findet eine neue Art: Schildkrötenenten");
        quatsch.add("Harry Potter erwacht aus Koma, erzählt Geschichten von Zauberern");
        quatsch.add("Das asoziale Netzwerk: Unterführung des Kängurus");
        quatsch.add("Großes wandelndes Schloss gesichtet");
        quatsch.add("Prinzessin Peach erneut entführt");
        quatsch.add("Dovahkiin verjagt Dinos aus Deanotopia");
        quatsch.add("Creeper reißt millionenschweres Loch in Pammeltons Verteidigung");
        quatsch.add("Großfamilie in Jojonien angeklagt: ungewollte Beschwörung Satans");
        quatsch.add("Kompletter Studiengang exmatrikuliert durch Mathe Professor");
        quatsch.add("Axel Dietze gewinnt Olympia Medaille!");
        quatsch.add("E Gaming wird in Olympia aufgenommen");
        quatsch.add("Fehlprdouktion von Gummischlangen verursacht Magendarmvirus");
    }

    public static void neueInfo(String inhalt){ //Eintragen und erzeugen von einer Wichtigen Nachricht
        Random rnd=new Random();
        Land land = Land.values()[inGame.getAmZug()];
        int i;
        if(land==Land.spieler || land.getErobertVon(Land.spieler)){
            i=(rnd.nextInt(3)+1);
            if(i< inLandSkill.getLvl()){
                nachrichten.add(new Nachricht(inhalt, inLand));
            }
        }else {
            i=(rnd.nextInt(4)+1);
            if(i< ausLandSkill.getLvl()) {
                nachrichten.add(new Nachricht(inhalt, ausLand));
            }
        }
    }

    public static void spassNachricht(){ //Erzeugen einer Nachricht
        Random rnd= new Random();
        int i= rnd.nextInt(2);
        if (i==0){
            if (quatsch.size()>0) {
                int eintragnr =rnd.nextInt(quatsch.size());
                String inhalt=quatsch.get(eintragnr);
                quatsch.remove(eintragnr);
                Nachricht n= new Nachricht(inhalt, spass);
                nachrichten.add(n);
            }
        }
    }

    public static void spezialNachricht(String inhalt) {
        nachrichten.add(new Nachricht(inhalt, spezial));
    }

    public static int getSize() {
        return nachrichten.size();
    }

    public static Nachricht getNachricht(int index) { //gibt nachrichten in umgekehrter Reihenfolge zurück (Logsystem)
        if (nachrichten.size()-index-1<0) return null;
        return nachrichten.get(nachrichten.size()-index-1);
    }

}
