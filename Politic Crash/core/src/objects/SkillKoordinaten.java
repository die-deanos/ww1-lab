package objects;

import java.util.HashMap;

public class SkillKoordinaten {

    private static HashMap<String,int[]> koordinaten = new HashMap<String, int[]>();

    public static void setEverything() {
        //Techskills Aktiv
        koordinaten.put("Auslandsspione1", new int[]{685, 668});
        koordinaten.put("Auslandsspione2", new int[]{685, 491});
        koordinaten.put("Auslandsspione3", new int[]{685, 315});

        koordinaten.put("Nachrichten1", new int[]{1093, 594});

        koordinaten.put("Firewall1", new int[]{1637, 578});
        koordinaten.put("Firewall2", new int[]{1637, 401});
        koordinaten.put("Firewall3", new int[]{1637, 224});

        koordinaten.put("Hacken1", new int[]{1503, 667});
        koordinaten.put("Hacken2", new int[]{1503, 491});
        koordinaten.put("Hacken3", new int[]{1503, 316});
        koordinaten.put("Hacken4", new int[]{1503, 140});

        koordinaten.put("Inlandsspione1", new int[]{550, 579});
        koordinaten.put("Inlandsspione2", new int[]{550, 402});
        koordinaten.put("Inlandsspione3", new int[]{550, 225});

        koordinaten.put("Langwellensensoren1", new int[]{290, 578});
        koordinaten.put("Langwellensensoren2", new int[]{290, 401});

        koordinaten.put("Sicherheitsdienst1", new int[]{425, 668});
        koordinaten.put("Sicherheitsdienst2", new int[]{425, 491});

        koordinaten.put("Unterhaltungsprogramm1", new int[]{1221, 669});
        koordinaten.put("Unterhaltungsprogramm2", new int[]{1221, 493});

        //Techskills Passiv
        koordinaten.put("Auslandsnachrichten1", new int[]{947, 668});
        koordinaten.put("Auslandsnachrichten2", new int[]{947, 491});
        koordinaten.put("Auslandsnachrichten3", new int[]{945, 315});
        koordinaten.put("Auslandsnachrichten4", new int[]{947, 140});

        koordinaten.put("Freies Internet1", new int[]{1377, 577});

        koordinaten.put("Inlandsnachrichten1", new int[]{818, 578});
        koordinaten.put("Inlandsnachrichten2", new int[]{818, 401});
        koordinaten.put("Inlandsnachrichten3", new int[]{818, 224});

        koordinaten.put("Kurzwellensensoren1", new int[]{164, 668});
        koordinaten.put("Kurzwellensensoren2", new int[]{164, 491});

        koordinaten.put("Fernsehen1", new int[]{1161, 819});

        koordinaten.put("Funkstelle1", new int[]{250, 819});

        koordinaten.put("Nachrichtendienst1", new int[]{889, 819});

        koordinaten.put("PC1", new int[]{1504, 819});

        koordinaten.put("Spione1", new int[]{551, 819});

        //Autokratie
        koordinaten.put("Mesias1", new int[]{1446, 476});
        koordinaten.put("Papst1", new int[]{755, 435});
        koordinaten.put("König1", new int[]{755+95, 435});
        koordinaten.put("Polygamie1", new int[]{1111, 583});
        koordinaten.put("Monogamie1", new int[]{1111+95, 583});

        //Demokratie
        koordinaten.put("Demokratie1", new int[]{1446, 476});
        koordinaten.put("Volksentscheid1", new int[]{755, 435});
        koordinaten.put("Wahlen1", new int[]{755+95, 435});
        koordinaten.put("Merkeln1", new int[]{1111, 583});
        koordinaten.put("Minderheitsregierung1", new int[]{1111+95, 583});

        //Diktatur
        koordinaten.put("Standpunkt des Obersten1", new int[]{1446, 476});
        koordinaten.put("EinHerrscherSystem1", new int[]{1111, 583});
        koordinaten.put("Einparteisystem1", new int[]{1111+95, 583});
        koordinaten.put("Waffengewalt1", new int[]{755, 435});
        koordinaten.put("Propaganda1", new int[]{755+95, 435});

        //Kommunismus
        koordinaten.put("Kommunistisches Manifest1", new int[]{1446, 476});
        koordinaten.put("Marktwirtschaft1", new int[]{755, 435});
        koordinaten.put("Staatswirtschaft1", new int[]{755+95, 435});
        koordinaten.put("Hinrichtung1", new int[]{1111, 583});
        koordinaten.put("Umerziehungsanstalt1", new int[]{1111+95, 583});

        koordinaten.put("Erobern1", new int[]{0, 0});
        koordinaten.put("Erpressen1", new int[]{0, 0});
        koordinaten.put("Schmeicheln1", new int[]{0, 0});
        koordinaten.put("Fragebögen1", new int[]{0, 0});
        koordinaten.put("Volksrede1", new int[]{0, 0});
    }

    public static int getX(String s,int level) {
        return koordinaten.get(s + level)[0];
    }
    public static int getY(String s,int level) {
        return koordinaten.get(s+level)[1];
    }
}
