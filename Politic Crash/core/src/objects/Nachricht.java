package objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Nachricht {

    private Color color;
    private String inhalt;

    public Nachricht(String inhalt, Color color){
        this.color = color;
        this.inhalt=inhalt;
    }

    public Color getColor() {
        return color;
    }

    public String getInhalt() {
        return inhalt;
    }
}
