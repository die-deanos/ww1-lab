package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import objects.SkillKoordinaten;
import screens.InGameScreen;
import screens.MainMenuScreen;

public class PoliticCrash extends Game {

	//Spritebatch
	private SpriteBatch batch; //eine Spritebatch für das gesammte Spiel, mehrere währen recourcen aufwendig, deswegen durchreichung der Variable

	//Screens
	private MainMenuScreen mainMenu; //MainMenuScreen nötig um ins Hauptmenu zurück zu kehren wenn man das Spiel beenden möchte
	private InGameScreen inGame; //InGameScreen, sehr wichtig für wechseln zwischen Screens

	//Aufzählungen für übersichtliche Case Behandlungen, public für alle Klassen verfügbar
	public enum Regierungsform {autokratie, demokratie, diktatur, kommunismus}
	public enum Gesinnung {agressiv, neutral, freundlich}
	public enum StatusRunde {warten, erfolg, fehlschlag, inaktiv}
	
	@Override
	public void create () {
		batch = new SpriteBatch(); //zeichnet Texturen (wird von jedem Screen verwendet)
		SkillKoordinaten.setEverything();
		mainMenu = new MainMenuScreen(this, batch); //Hauptmenuscreen erstellen
		setScreen(mainMenu); //Screen als aktiven Screen einstellen
	}
	
	@Override
	public void dispose () { //Auflösen aller Spielobjekte, insbesondere Texturen aus Grafikspeicher
		mainMenu.dispose();
		if(inGame!=null) inGame.dispose();
	}

	//Getter / Setter
	public MainMenuScreen getMainMenu() {
		return mainMenu;
	}

	public InGameScreen getInGame() {
		return inGame;
	}

	public void setInGame(InGameScreen screen) {
		this.inGame = screen;
	}
}
