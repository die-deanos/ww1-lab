package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.PoliticCrash;

import objects.Aktiv;
import objects.Land;

public class MenuLandScreen implements Screen {

	private PoliticCrash game;
	private SpriteBatch batch;
	private Land land;
	private Land zugriffsLand;
	private Texture hintergrund;
	private Sprite zurueck;
	private BitmapFont font32, font44;
	
	public MenuLandScreen(PoliticCrash game, SpriteBatch batch, Land land) { //Konstruktor
		this.game = game;
		this.batch = batch;
		this.land = land;
	}

	@Override
	public void show() {

		//Eroberte Länder können diesen Screen auch aufrufen, Anzeige deshalb abhängig von amZug
		zugriffsLand = Land.values()[game.getInGame().getAmZug()]; //Updaten bei jedem Zugriff

		if(land!=Land.spieler) hintergrund = new Texture("InGame/Land/"+land.getName()+".png");
		else hintergrund = new Texture(Gdx.files.local("core\\assets\\InGame\\Land\\Spielerland.png"));

		zurueck = new Sprite(new Texture("InGame/Land/Zurueck.png"));
		zurueck.setSize(1920, 99);
		zurueck.setPosition(0, 0);

		font32 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_32.fnt"));
		font44 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_44.fnt"));
	}

	@Override
	public void render(float delta) {
		batch.begin();
		batch.draw(hintergrund, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		zurueck.draw(batch);
		
		int x=560; int y=800; int w=400; int h=40;
		for(Aktiv s : zugriffsLand.getSkillbaum().getAktiv()) {
			//freigeschaltet und entweder skill für Inland gedacht + eigenes Land, oder skill für ausland gedacht + anders Land
			if (s.getFreigeschaltet() && (s.getEigen() == land.getErobertVon(zugriffsLand))) {
				//Skill Zeichnen
				s.getButton().setPosition(x, y);
				s.getButton().setSize(w, h);
				s.getButton().draw(batch);
				font32.draw(batch,s.getName(), x+14,y+30);
				y-=51;
			}
		}
		for(Aktiv s : zugriffsLand.getSkillbaum().getAktiv()) {
			//freigeschaltet und entweder skill für Inland gedacht + eigenes Land, oder skill für ausland gedacht + anders Land
			if (s.getFreigeschaltet() && (s.getEigen() == land.getErobertVon(zugriffsLand))) {
				//Skill ausfuehren wenn angeklicked
				if(s.getButton().getBoundingRectangle().contains(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY())) {
					if(Gdx.input.justTouched() && !land.getAktiveSkills().contains(s)) {
						zugriffsLand.initiateSkill(s, land);
					}
					game.getInGame().drawSkillInfo(s,s.getLvl(),false);
				}
			}
		}
		batch.end();
		
		if(zurueck.getBoundingRectangle().contains(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY()) && Gdx.input.isTouched()) {
			game.setScreen(game.getInGame());
		}

	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}
	
	@Override
	public void dispose() {
		
	}

}
