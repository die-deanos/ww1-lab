package screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.PoliticCrash;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class MainMenuScreen extends InputAdapter implements Screen{

	//Standartvariablen
	PoliticCrash game; //Spielvariable, zum ändern und Speichern der Screens
	SpriteBatch batch; //Spritebatch, zum Zeichnen alles Grafiken auf dem Bildschirm
	float scale = Gdx.graphics.getWidth()/1920f; //für Bildschirme mit einer anderen Auflösung als FullHD

	//Grafikvariablen
	Sprite neuesSpiel, ladeSpiel, credits, beendeSpiel; //Sprites, Texturen mit Koordinaten und Größe, besitzen Methode zur Klickabfrage
	Texture hintergrund; //simple Textur für Hintergrund

	/** Konstruktor */
	public MainMenuScreen(PoliticCrash game, SpriteBatch batch) { //Konstruktor
		this.game = game;
		this.batch = batch;
	}

	/** LibGdx interner Aufruf wenn Screen zum aktiven Screen wird */
	public void show() {

		//Rechenvariablen
		int zwischenAbstand = (int) (150*scale); //Abstand zwischen einzelnen Buttons
		int untererAbstand = (int) (192*scale); //Abstand zwischen unteren und unterstem Button

		//Texturen
		hintergrund = new Texture("MainMenu\\Landkarte.png");

		//Sprites
		neuesSpiel = new Sprite(new Texture("MainMenu\\NewGame.png"));
		neuesSpiel.setSize(800*scale, 100*scale);
		neuesSpiel.setPosition((Gdx.graphics.getWidth()- neuesSpiel.getWidth())/2, untererAbstand+ zwischenAbstand *3);
		
		ladeSpiel = new Sprite(new Texture("MainMenu\\LoadGame.png"));
		ladeSpiel.setSize(800*scale, 100*scale);
		ladeSpiel.setPosition((Gdx.graphics.getWidth()- ladeSpiel.getWidth())/2, untererAbstand+ zwischenAbstand *2);
		
		credits = new Sprite(new Texture("MainMenu\\Credits.png"));
		credits.setSize(600*scale, 100*scale);
		credits.setPosition((Gdx.graphics.getWidth()-credits.getWidth())/2, untererAbstand+ zwischenAbstand);
		
		beendeSpiel = new Sprite(new Texture("MainMenu\\End.png"));
		beendeSpiel.setSize(400*scale, 100*scale);
		beendeSpiel.setPosition((Gdx.graphics.getWidth()- beendeSpiel.getWidth())/2, untererAbstand);
		
	}

	/** LibGdx interner Aufruf zum rendern des Screens */
	public void render(float delta) { //LibGdx interner Aufruf zum Zeichnen des Frames

		if (Gdx.input.justTouched()) { //Wenn geklickt wurde
			if (neuesSpiel.getBoundingRectangle().contains(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY())) { //Cursor über NeuesSpielButton
				game.setScreen(new LandErstellungsScreen(game, batch));
			}
			else if (beendeSpiel.getBoundingRectangle().contains(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY())) { //Cursor über SpielBeendenButton
				game.dispose();
				Gdx.app.exit();
			}
		}


		batch.begin(); //Batch beginnt zu zeichnen

		//Texturen brauchen Koordinaten und Höhe/Breite
		batch.draw(hintergrund, 0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

		//Sprites kennen diese bereits und bekommen lediglich den Zeichenbefehl
		neuesSpiel.draw(batch);
		ladeSpiel.draw(batch);
		credits.draw(batch);
		beendeSpiel.draw(batch);

		batch.end(); //Batch beendet den Zeichenprozess und zeigt Endresultat an
	}

	/** Ungenutzt da fullscreen */
	public void resize(int width, int height) {
		
	}

	public void pause() {
		
	}

	public void resume() {
		
	}

	public void hide() {
		
	}

	/** Spezielles entfernen für Texturen aus dem Grafikspeicher */
	public void dispose() {
		hintergrund.dispose();
		neuesSpiel.getTexture().dispose();
		ladeSpiel.getTexture().dispose();
		credits.getTexture().dispose();
		beendeSpiel.getTexture().dispose();
	}

}
