package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.PoliticCrash;
import com.mygdx.game.PoliticCrash.Regierungsform;
import objects.Land;
import objects.Skill;

import java.util.ArrayList;

public class PolitikbaumScreen implements Screen {

    PoliticCrash game;
    SpriteBatch batch;

    Land land;
    ArrayList<Skill> politisch;
    Regierungsform regierungsform;

    Texture hintergrund, overlay, top,  symbol;
    Texture[] transparent=new Texture[3], solide=new Texture[3];
    Sprite zurueck;

    /** Konstruktor */
    PolitikbaumScreen(PoliticCrash game, SpriteBatch batch) {
        this.game = game;
        this.batch = batch;
    }

    /** LibGdx interner Aufruf wenn Screen zum aktiven Screen wird */
    public void show() {

        //Eroberte Länder können diesen Screen auch aufrufen, Update bei jedem Zug
        land = Land.values()[game.getInGame().getAmZug()];
        this.politisch = land.getSkillbaum().getPolitisch();
        this.regierungsform=land.getSkillbaum().getRegierungsform();

        hintergrund = new Texture("MainMenu\\Landkarte.png");
        overlay = new Texture("InGame\\Skilltree\\Politisch.png");
        solide[0] = new Texture("InGame\\Skilltree\\Symbole\\SolidLeft.png");
        solide[1] = new Texture("InGame\\Skilltree\\Symbole\\SolidRight.png");
        solide[2] = new Texture("InGame\\Skilltree\\Symbole\\SolidBig.png");
        transparent[0] = new Texture("InGame\\Skilltree\\Symbole\\BlankLeft.png");
        transparent[1] = new Texture("InGame\\Skilltree\\Symbole\\BlankRight.png");
        transparent[2] = new Texture("InGame\\Skilltree\\Symbole\\BlankBig.png");

        //Label und Symbol in Abhängigkeit der Regierungsform
        String temp;
        if (regierungsform==Regierungsform.autokratie) temp="Autokratie";
        else if (regierungsform==Regierungsform.demokratie) temp="Demokratie";
        else if (regierungsform==Regierungsform.diktatur) temp="Diktatur";
        else temp="Kommunismus";
        top = new Texture("InGame\\Skilltree\\"+temp+"Top.png");
        symbol = new Texture("InGame\\Skilltree\\"+temp+".png");


        //Sprites
        zurueck = new Sprite(new Texture("InGame/Land/Zurueck.png"));
        zurueck.setSize(1920, 99);
        zurueck.setPosition(0, 0);
    }

    /** LibGdx interner Aufruf zum rendern des Screens */
    public void render(float delta) {

        boolean clicked = Gdx.input.justTouched(); //wenn in diesem Frame geclicked wurde

        batch.begin();
        batch.draw(hintergrund,0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.draw(overlay,0,0,Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.draw(top,0,Gdx.graphics.getHeight()-100, Gdx.graphics.getWidth(), 100); //Label des Skilltrees
        batch.draw(symbol,300,441, 220, 220);

        int lrindex=0; //0 oder 1 für links/ rechts

        for (Skill skill : politisch) { //alle Sprites zeichnen für aktive Skills
            for (int n = 0; n < skill.getMaxlvl(); n++) { //für alle Level

                Sprite sp = skill.getSprites()[n];
                Texture t;
                float sichtbarkeit;

                if(n<skill.getLvl()) {
                    if(sp.getWidth()<sp.getHeight()) {
                        t=solide[lrindex];
                        lrindex=(lrindex+1)%2;
                    }
                    else t=solide[2];
                    sichtbarkeit=1;
                }
                else {
                    if(sp.getWidth()<sp.getHeight()) {
                        t=transparent[lrindex];
                        lrindex=(lrindex+1)%2;

                    }
                    else t=transparent[2];
                    sichtbarkeit=0.25f;
                }
                batch.draw(t,sp.getX(),sp.getY(),sp.getWidth(),sp.getHeight());
                skill.getSprites()[n].draw(batch,sichtbarkeit); //Sprite zeichnen
            }
        }
        zurueck.draw(batch);

        //Informationsfenster und Hitdetektion
        for (Skill skill : politisch) { //für jeden skill
            boolean exit = false; //zum verlassen der doppelten forschleife
            for (int i=0; i<skill.getMaxlvl(); i++) { //für jeden Sprite
                //wenn Maus über Skill
                if(skill.getSprites()[i].getBoundingRectangle().contains(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY())) {
                    exit = true;
                    if (clicked && i == skill.getLvl()) {
                        land.levelSkill(skill);
                    }

                    //Draw Stats
                    game.getInGame().drawSkillInfo(skill, i, true);

                }
                if (exit) break;
            }
            if (exit) break;
        }

        if(zurueck.getBoundingRectangle().contains(Gdx.input.getX(),1080-Gdx.input.getY()) && Gdx.input.isTouched()) {
            game.setScreen(game.getInGame());
        }
        batch.end();

    }

    /** Ungenutzt da fullscreen */
    public void resize(int width, int height) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void hide() {

    }

    /** Spezielles entfernen für Texturen aus dem Grafikspeicher */
    public void dispose() {
        if(hintergrund !=null){
            hintergrund.dispose();
            overlay.dispose();
            top.dispose();
            symbol.dispose();
            transparent[0].dispose(); transparent[1].dispose(); transparent[2].dispose();
            solide[0].dispose(); solide[1].dispose(); solide[2].dispose();
            zurueck.getTexture().dispose();
        }
    }
}
