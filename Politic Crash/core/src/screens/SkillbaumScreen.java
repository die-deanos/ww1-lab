package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.PoliticCrash;
import objects.Land;
import objects.Skill;

import java.util.ArrayList;

public class SkillbaumScreen implements Screen {

	PoliticCrash game;
	SpriteBatch batch;

	Land land;
	ArrayList<Skill> technisch;

	Texture hintergrund, overlay, top, transparent, solide;
	Sprite zurueck;

	/** Konstruktor */
	public SkillbaumScreen(PoliticCrash game, SpriteBatch batch) { //Konstruktor
		this.game = game;
		this.batch = batch;
	}

	/** LibGdx interner Aufruf wenn Screen zum aktiven Screen wird */
	public void show() {

		//Eroberte Länder können diesen Screen auch aufrufen, Update bei jedem Zugriff
		land = Land.values()[game.getInGame().getAmZug()];
		technisch = land.getSkillbaum().getTechnisch();

		hintergrund = new Texture("MainMenu\\Landkarte.png");
		overlay = new Texture("InGame\\Skilltree\\Technisch.png");
		top = new Texture("InGame\\Skilltree\\TechnischTop.png");
		solide = new Texture("InGame\\Skilltree\\Symbole\\SolidSmall.png");
		transparent = new Texture("InGame\\Skilltree\\Symbole\\BlankSmall.png");

		zurueck = new Sprite(new Texture("InGame/Land/Zurueck.png"));
		zurueck.setSize(1920, 99);
		zurueck.setPosition(0, 0);
	}

	/** LibGdx interner Aufruf zum rendern des Screens */
	public void render(float delta) {

		boolean clicked = Gdx.input.justTouched(); //wenn in diesem Frame geclicked wurde

		batch.begin();
		batch.draw(hintergrund,0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //Hintergrund
		batch.draw(overlay,0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //Linien zwischen Skills
		batch.draw(top,0,Gdx.graphics.getHeight()-100, Gdx.graphics.getWidth(), 100); //Label des Skilltrees

		for (Skill skill : technisch) { //alle Sprites zeichnen für aktive Skills
			for (int n = 0; n < skill.getMaxlvl(); n++) { //für alle Level

				Texture t; //mit welcher Textur
				float sichtbarkeit; //mit welcher sichtbarkeit

				if(n<skill.getLvl()) { //wenn freigeschaltet
					t = solide;
					sichtbarkeit=1;
				}
				else {
					t = transparent;
					sichtbarkeit=0.25f;
				}

				Sprite sp = skill.getSprites()[n];
				batch.draw(t,sp.getX(),sp.getY(),sp.getWidth(),sp.getHeight()); //Hintergrund für Icon
				skill.getSprites()[n].draw(batch,sichtbarkeit); //Icon zeichnen
			}
		}
		zurueck.draw(batch);

		//Informationsfenster und Hitdetektion
		for (Skill skill : technisch) { //Informationen und Klickabfrage für jeden skill
			boolean exit = false; //zum verlassen der doppelten forschleife
			for (int i=0; i<skill.getMaxlvl(); i++) {
				//Custom Hitdetection für Kreis
				Sprite sp = skill.getSprites()[i];
				int abstandX = (int) Math.abs(Gdx.input.getX()-(sp.getX()+sp.getWidth()/2));
				int abstandY = (int) Math.abs((Gdx.graphics.getHeight()-Gdx.input.getY())-(sp.getY()+sp.getHeight()/2));
				double abstandR = Math.sqrt(abstandX*abstandX + abstandY*abstandY);
				if(abstandR<sp.getHeight()/2) { //Cursor über Sprite des Skills
					exit = true;
					if (clicked && i == skill.getLvl()) {
						land.levelSkill(skill);
					}
					game.getInGame().drawSkillInfo(skill,i, true);

				}
				if (exit) break;
			}
			if (exit) break;
		}

		if(zurueck.getBoundingRectangle().contains(Gdx.input.getX(),1080-Gdx.input.getY()) && Gdx.input.isTouched()) {
			game.setScreen(game.getInGame());
		}
		batch.end();
	}

	/** Ungenutzt da fullscreen */
	public void resize(int width, int height) {
		
	}

	public void pause() {
		
	}

	public void resume() {
		
	}

	public void hide() {
		
	}

	/** Spezielles entfernen für Texturen aus dem Grafikspeicher */
	public void dispose() {
		if(hintergrund!=null) { //Spiel kann beendet werden ohne Show aufzurufen, daher nicht zwangsläufig instanziiert
			hintergrund.dispose();
			overlay.dispose();
			top.dispose();
			transparent.dispose();
			solide.dispose();
			zurueck.getTexture().dispose();
		}
	}
}

