package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import com.mygdx.game.PoliticCrash;
import objects.Land;

public class LandErstellungsScreen implements Screen, InputProcessor {

    //Standartvariablen
    PoliticCrash game; //Spielvariable, zum ändern und Speichern der Screens
    SpriteBatch batch; //Spritebatch, zum Zeichnen alles Grafiken auf dem Bildschirm
    float scale = Gdx.graphics.getWidth()/1920f; //für Bildschirme mit einer anderen Auflösung als FullHD

    //Allgemein
    private Sprite fertig; //um Spiel zu starten
    private Texture hintergrund, überschriften; //Texturen für Hintergrund und Überschriften

    //Auswahl
    private enum Auswahl{ //interaktion über mehrere Frames hinweg (Texteditoren und Zeichentools)
        beschreibung,name, //Text
        farbe,helligkeit,oberhaupt; //Zeichnen

        private Sprite sprite; //Sprite der Auswahl

        //Getter/Setter
        public Sprite getSprite() {return sprite;}
        public void setSprite(Sprite sprite) {this.sprite=sprite;}
        public boolean getHit(int x, int y) {
            if(sprite.getBoundingRectangle().contains(x,Gdx.graphics.getHeight()-y)) return true;
            else return false;
        }
    }
    private Auswahl auswahl=Auswahl.name; //Pointer für aktuelle Auswahl

    //TextVariablen
    private int timer=0, indexBeschreibung=0; //Timer zum Anzeigen des Cursors, index für ausgewählte Zeile
    private BitmapFont font32, font44, font80W, font80B; //zum rendern von Schrift, verschien große Versionen der selben Schriftart, für Beschreibung, Namen und große Überschrift
    private GlyphLayout layout; //Objekt zum berechnen der Pixelgröße von
    private String nameText; //selbstwählbarer Name des eigenen Lands
    private String[] beschreibungText; //selbst einstellbare Beschreibung des Landes, jedes Element ist eine neue Zeile
    private String[] regierungStrings = {"Autokratie", "Demokratie", "Diktatur", "Kommunismus"}; //String zum Anzeigen des Namen der Regierungsformen, anstatt einer getName() in der enum

    //Zeichenvariablen
    private int radius=6; //Radius des Pinsels / Radierers
    private float lastX, lastY, //Position des Cursors im letzten Frame, zum Zeichnen durchgängiger Linien
            helligkeit =0f; //eingestellte Helligkeit
    private Texture cursor; //Anzeigen der eingestellten Helligkeit
    private Sprite flagge; //Auswählen der Länderfarbe beim Anklicken
    private Pixmap oberhauptPix, //zum Zeichnen des eigenen Anführers
            farbePix, //Pixmap des Farbfensters zum bekommen der Farbe an einer bestimmten Position
            vorschauPix, //Vorschau der Pinselgröße und -farbe
            overlayPix; //einstellbarer Farbiger Hintergrund
    private Texture[] pixCache = new Texture[3]; //Speichern temporärer Texturen um sie am ende des Frames zu disposen, verhindert fluten des Arbeitsspeichers ==> halbierte Framerate, extremst wichtig
    private Color farbeAuswahl, farbeGemischt, farbeLand; //Farben für Auswahl aus farbePix, gemischte Farbe mit helligkeit und die Farbe des Landes

    private enum Werkzeug { //Werkzeugkasten, equivalent zu Auswahl, allerdings abgetrennt, da Nutzung unabhängig und nicht über mehrere Frames
        radierer, pinsel, pipette;
        Sprite sprite; //Sprite der Auswahl
        //Getter/Setter
        public Sprite getSprite() {return sprite;}
        public void setSprite(Sprite sprite) {this.sprite=sprite;}
        public boolean getHit(int x, int y) {
            if(sprite.getBoundingRectangle().contains(x,Gdx.graphics.getHeight()-y)) return true;
            else return false;
        }
    }
    Werkzeug werkzeug = Werkzeug.pinsel; //ausgewähltes Werkzeug

    //Allignment und Regierungsform
    private int indexAllignment=1, indexRegierung=0; //Index für ausgewählte Option
    private Sprite links1, rechts1, links2, rechts2; //Pfeilsprites für Auswahl (Allignment, Regierung)
    private Texture[] allignment = new Texture[3], regierungsform = new Texture[4]; //mögliche Anzeigeoptionen als Array


    /** Konstruktor */
    public LandErstellungsScreen(PoliticCrash game, SpriteBatch batch) {
        this.game = game;
        this.batch = batch;

        //Standartfarbwerte
        farbeAuswahl = new Color(1,1,1,1);
        farbeGemischt = new Color(farbeAuswahl.r* helligkeit, farbeAuswahl.g* helligkeit, farbeAuswahl.b* helligkeit,1);

        //Strings Instanziieren
        beschreibungText = new String[11];
        for(int c = 0; c< beschreibungText.length; c++) {
            beschreibungText[c]="";
        }
        nameText ="Spielerland";

        //Inputprozessor für automatischen Methodenaufruf (Siehe ab Zeile 337)
        Gdx.input.setInputProcessor(this);
    }

    /** LibGdx interner Aufruf wenn Screen zum aktiven Screen wird */
    public void show() {

        //Texturen
        hintergrund = new Texture("MainMenu\\Landkarte.png");
        cursor = new Texture("Erstellung\\PfeilHoch.png");
        überschriften = new Texture("Erstellung\\Uberschriften.png");

        allignment[0] = new Texture("Erstellung\\VerhaltenAggressiv.png");
        allignment[1] = new Texture("Erstellung\\VerhaltenNeutral.png");
        allignment[2] = new Texture("Erstellung\\VerhaltenFriedlich.png");

        regierungsform[0] = new Texture("Erstellung\\RegierungsformAutokratie.png");
        regierungsform[1] = new Texture("Erstellung\\RegierungsformDemokratie.png");
        regierungsform[2] = new Texture("Erstellung\\RegierungsformDiktatur.png");
        regierungsform[3] = new Texture("Erstellung\\RegierungsformKommunismus.png");

        //Sprites Allignment / Regierungsform
        links1 = new Sprite(new Texture("Erstellung\\PfeilLinks.png"));
        links1.setPosition(47*scale, 323*scale);
        links1.setSize(65*scale, 100*scale);

        rechts1 = new Sprite(new Texture("Erstellung\\PfeilRechts.png"));
        rechts1.setPosition(550*scale, 323*scale);
        rechts1.setSize(65*scale, 100*scale);

        links2 = new Sprite(new Texture("Erstellung\\PfeilLinks.png"));
        links2.setPosition(47*scale, 137*scale);
        links2.setSize(65*scale, 100*scale);

        rechts2 = new Sprite(new Texture("Erstellung\\PfeilRechts.png"));
        rechts2.setPosition(550*scale, 137*scale);
        rechts2.setSize(65*scale, 100*scale);

        //Sprites Text
        Auswahl.beschreibung.setSprite(new Sprite(new Texture("Erstellung\\Beschreibung.png")));
        Auswahl.beschreibung.getSprite().setPosition(131*scale, 443*scale);
        Auswahl.beschreibung.getSprite().setSize(400*scale, 397*scale);

        Auswahl.name.setSprite(new Sprite(new Texture("Erstellung\\Name.png")));
        Auswahl.name.getSprite().setPosition(558*scale, 718*scale);
        Auswahl.name.getSprite().setSize(400*scale, 122*scale);


        //Sprites Zeichnen
        Auswahl.oberhaupt.setSprite(new Sprite(new Texture("Erstellung\\Oberhaupt.png")));
        Auswahl.oberhaupt.getSprite().setPosition(1288*scale,240*scale);
        Auswahl.oberhaupt.getSprite().setSize(500*scale, 600*scale);

        Auswahl.farbe.setSprite(new Sprite(new Texture("Erstellung\\FarbenRahmen.png")));
        Auswahl.farbe.getSprite().setPosition(982*scale, 544*scale);
        Auswahl.farbe.getSprite().setSize(296*scale, 296*scale);

        Auswahl.helligkeit.setSprite(new Sprite(new Texture("Erstellung\\Helligkeit.png")));
        Auswahl.helligkeit.getSprite().setPosition(992*scale, 484*scale);
        Auswahl.helligkeit.getSprite().setSize(276*scale, 50*scale);

        Werkzeug.pinsel.setSprite(new Sprite(new Texture("Erstellung\\BoxPinsel.png")));
        Werkzeug.pinsel.getSprite().setPosition(1228*scale,360*scale);
        Werkzeug.pinsel.getSprite().setSize(50*scale, 50*scale);

        Werkzeug.radierer.setSprite(new Sprite(new Texture("Erstellung\\BoxRadierer.png")));
        Werkzeug.radierer.getSprite().setPosition(1228*scale,300*scale);
        Werkzeug.radierer.getSprite().setSize(50*scale,50*scale);

        Werkzeug.pipette.setSprite(new Sprite(new Texture("Erstellung\\BoxPipette.png")));
        Werkzeug.pipette.getSprite().setPosition(1228*scale,240*scale);
        Werkzeug.pipette.getSprite().setSize(50*scale,50*scale);

        //Beenden Sprite
        fertig = new Sprite(new Texture("Erstellung\\Fertig.png"));
        fertig.setSize(Gdx.graphics.getWidth(), 100*scale);
        fertig.setPosition(0,0);


        flagge = new Sprite(new Texture("Erstellung\\Flagge.png"));
        flagge.setSize(Gdx.graphics.getWidth(), 100*scale);
        flagge.setPosition(0, Gdx.graphics.getHeight()-flagge.getHeight());

        //Pixmap
        oberhauptPix = new Pixmap((int)(500*scale), (int)(600*scale), Pixmap.Format.RGBA8888);
        oberhauptPix.setBlending(Pixmap.Blending.None);
        oberhauptPix.setColor(new Color(0,0,0,0));
        oberhauptPix.fill();
        oberhauptPix.setColor(farbeGemischt);

        Texture f = new Texture("Erstellung\\FarbenAuswahl.png");
        f.getTextureData().prepare();
        farbePix = f.getTextureData().consumePixmap();
        f.dispose();

        vorschauPix = new Pixmap((int)(61*scale), (int)(61*scale), Pixmap.Format.RGBA8888);
        vorschauPix.setBlending(Pixmap.Blending.None);
        updateVorschau();

        overlayPix = new Pixmap(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),Pixmap.Format.RGBA8888);
        overlayPix.setBlending(Pixmap.Blending.None);
        updateOverlay(new Color(1,1,1,1));

        //Fontrenderer
        font32 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_32.fnt"));
        font44 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_44.fnt"));
        font80W = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_80_White.fnt"));
        font80B = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_80_Black.fnt"));
    }

    /** LibGdx interner Aufruf zum rendern des Screens */
    public void render(float delta) {

        batch.begin();

        //Hintergrund
        batch.draw(hintergrund,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        pixCache[0]=new Texture(overlayPix);
        batch.draw(pixCache[0],0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        batch.draw(überschriften,127*scale,859*scale,1531*scale,49*scale);

        //Links auf Screen
            //Beschreibung und Namen mit Text
            Auswahl.name.getSprite().draw(batch); //Namensfenster
            Auswahl.beschreibung.getSprite().draw(batch); //Beschreibungsfenster
            timer=(timer+1)%80; //Timer erhöhen für cursor
            String tempStr;
            for(int c = 0; c< beschreibungText.length; c++) {
                tempStr = beschreibungText[c];
                if(auswahl==Auswahl.beschreibung && c==indexBeschreibung && timer>40) tempStr+="|";
                font32.draw(batch, tempStr, 163*scale,(800-30*c)*scale);
            }
            tempStr = nameText;
            if(auswahl==Auswahl.name && timer>40) tempStr+="|";
            font44.draw(batch, tempStr, 591*scale,790*scale);

            //Allignment
            batch.draw(allignment[indexAllignment],131*scale, 275*scale,400*scale, 168*scale);
            links1.draw(batch);
            rechts1.draw(batch);

            //Regierungsform
            batch.draw(regierungsform[indexRegierung],131*scale,126*scale,400*scale,122*scale);
            links2.draw(batch);
            rechts2.draw(batch);

        //Rechts auf Screen
            //Zeichenwerkzeuge
            Auswahl.farbe.getSprite().draw(batch);
            Auswahl.helligkeit.getSprite().draw(batch);
            batch.draw(cursor,986*scale +((Auswahl.helligkeit.getSprite().getWidth()-20*scale)* helligkeit),457*scale,32*scale,17*scale);
            Werkzeug.pinsel.getSprite().draw(batch);
            Werkzeug.radierer.getSprite().draw(batch);
            Werkzeug.pipette.getSprite().draw(batch);

            //Oberhaupt (Canvas)
            pixCache[1]=new Texture(oberhauptPix);
            Sprite temp = Auswahl.oberhaupt.getSprite();
            batch.draw(pixCache[1], temp.getX(), temp.getY(), temp.getWidth(), temp.getHeight());
            Auswahl.oberhaupt.getSprite().draw(batch);

        //Overlay
        fertig.draw(batch);
        flagge.draw(batch);
        tempStr = regierungStrings[indexRegierung]+": "+nameText;
        layout = new GlyphLayout(font80W, tempStr);
        if ((farbeLand.r+ farbeLand.g+ farbeLand.b)/3<0.666f) font80W.draw(batch,tempStr,(Gdx.graphics.getWidth()-layout.width)/2,1053*scale);
        else font80B.draw(batch,tempStr,(Gdx.graphics.getWidth()-layout.width)/2,1053*scale);


        //Forschau für Zeichenwerkzeug
        if(Gdx.input.getX()>Gdx.graphics.getWidth()/2) {
            pixCache[2]=new Texture(vorschauPix);
            batch.draw(pixCache[2], Gdx.input.getX()-30*scale, Gdx.graphics.getHeight()-Gdx.input.getY()-30*scale);
        }
        else pixCache[2]=null;
        batch.end();

        //aus Pixmaps erstellte Texturen disposen (wird dies nicht gemacht fluten diese den RAM)
        pixCache[0].dispose();
        pixCache[1].dispose();
        if(pixCache[2]!=null) pixCache[2].dispose();

    }

    /** Ungenutzt da fullscreen */
    public void resize(int width, int height) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void hide() {

    }

    /** Spezielles entfernen für Texturen aus dem Grafikspeicher */
    public void dispose() {
        for(Auswahl a : Auswahl.values()) {
            a.getSprite().getTexture().dispose();
        }
        for(Werkzeug w : Werkzeug.values()) {
            w.getSprite().getTexture().dispose();
        }

        //Fontrenderer
        font32.dispose();
        font44.dispose();

        //Pixmaps
        oberhauptPix.dispose();
        farbePix.dispose();
        vorschauPix.dispose();
        overlayPix.dispose();

        //Sprites
        links1.getTexture().dispose();
        links2.getTexture().dispose();
        rechts1.getTexture().dispose();
        rechts2.getTexture().dispose();
        fertig.getTexture().dispose();
        flagge.getTexture().dispose();

        //Texturen
        hintergrund.dispose();
        cursor.dispose();
        überschriften.dispose();
        for(Texture t : allignment) {t.dispose();}
    }

    //Inputprozessor, automatischer aufruf durch libgdx

    /** Aufruf wenn Taste gerade gedrückt */
    public boolean keyDown(int keycode) {

        if(keycode==Input.Keys.BACKSPACE) { //für Backspace (letztes Zeichen löschen)

            if(auswahl==Auswahl.name) { //für Name ausgewählt
                if(nameText.length()>0) { //nicht für lehren String
                    nameText = nameText.substring(0, nameText.length()-1); //letztes Zeichen entfernen
                }
            }
            else if(auswahl==Auswahl.beschreibung){ //für Beschreibung ausgewählt
                if(beschreibungText[indexBeschreibung].length()>0) { //nicht für lehren String
                    beschreibungText[indexBeschreibung]=beschreibungText[indexBeschreibung].substring(0, beschreibungText[indexBeschreibung].length()-1); //letztes Zeichen entfernen
                }
                else if(indexBeschreibung>0 && beschreibungText[indexBeschreibung-1].length()>0) { //in Zeile davor springen wenn möglich
                    indexBeschreibung--;
                    keyDown(keycode);
                }
            }
        }
        else if(auswahl==Auswahl.beschreibung) { //Für Beschreibung hoch und runterspringen, falls entsprechende Eingabe
            if(keycode==Input.Keys.UP) {
                if(indexBeschreibung>0) indexBeschreibung--;
            }
            else if(keycode==Input.Keys.DOWN) {
                if(indexBeschreibung< beschreibungText.length-1) indexBeschreibung++;
            }
        }
        return true;
    }

    /** Aufruf wenn Taste gerade gelößt wurde */
    public boolean keyUp(int keycode) {
        return false;
    }

    /** Aufruf wenn getippt wurde */
    public boolean keyTyped(char character) {

        String s = ""+character;
        if(s.matches("[\\w ]") || s.matches("[-_,;.:!?+*/%$§()=]")) { //character ist entweder Wort (\w), Lehrzeichen( ) oder Spezialzeichen

            if(auswahl==Auswahl.name) {
                //Länge des neuen Strings in Pixeln überprüfen
                String temp = nameText +character;
                GlyphLayout layout = new GlyphLayout(font44, temp);
                if (layout.width<335) nameText =temp;
            }
            else if(auswahl==Auswahl.beschreibung){
                //Länge des neuen Strings in Pixeln überprüfen
                String temp = beschreibungText[indexBeschreibung]+character;
                GlyphLayout layout = new GlyphLayout(font32, temp);
                if (layout.width<335) beschreibungText[indexBeschreibung]=temp;

                //wenn nächste Zeile leer => dort hinein schreiben
                else if(indexBeschreibung< beschreibungText.length-1 && beschreibungText[indexBeschreibung+1].equals("")) {
                    indexBeschreibung++;
                    keyTyped(character);
                }
            }
            return true;
        }
        else return false;
    }

    /** Aufruf wenn Maustaste gerade gedrückt */
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        //reset der letzten Position am anfang einer neuen Linie (wenn noch nicht gedragged wird)
        if (Auswahl.oberhaupt.getHit(screenX,screenY)) {
            auswahl=Auswahl.oberhaupt;

            if(werkzeug==Werkzeug.pinsel || werkzeug==Werkzeug.radierer) {
                lastX=screenX - Auswahl.oberhaupt.getSprite().getX();
                lastY=screenY - Auswahl.oberhaupt.getSprite().getY();
                drawCircle((int) (screenX-Auswahl.oberhaupt.getSprite().getX()),(int)(screenY- Auswahl.oberhaupt.getSprite().getY()), radius);
            }
            else pickColor(screenX,screenY);

        }
        //Farbauswahl
        else if(Auswahl.farbe.getHit(screenX,screenY)) {
            auswahl=Auswahl.farbe;
            pickColor(screenX,screenY);
        }
        //Helligkeitsauswahl
        else if(Auswahl.helligkeit.getHit(screenX,screenY)) {
            auswahl=Auswahl.helligkeit;
            pickBrightness(screenX);
        }
        else if(Auswahl.name.getHit(screenX,screenY)) {
            auswahl=Auswahl.name;
        }
        else if(Auswahl.beschreibung.getHit(screenX,screenY)) {
            auswahl=Auswahl.beschreibung;
        }
        else if(Werkzeug.radierer.getHit(screenX,screenY)) {
            werkzeug=Werkzeug.radierer;
            updateVorschau();
        }
        else if(Werkzeug.pinsel.getHit(screenX,screenY)) {
            werkzeug=Werkzeug.pinsel;
            updateVorschau();
        }
        else if(Werkzeug.pipette.getHit(screenX,screenY)) {
            werkzeug=Werkzeug.pipette;
            updateVorschau();
        }
        else if(flagge.getBoundingRectangle().contains(screenX, Gdx.graphics.getHeight()-screenY)) {
            updateOverlay(farbeGemischt);
        }


        //Pfeile für Allignment
        else if (rechts1.getBoundingRectangle().contains(screenX, Gdx.graphics.getHeight()-screenY)) {
            indexAllignment = (indexAllignment + 1) % 3;
        } else if (links1.getBoundingRectangle().contains(screenX, Gdx.graphics.getHeight()-screenY)) {
            indexAllignment = Math.floorMod(indexAllignment - 1, 3); //negativer Modulo, daher Math.floorMod
        }
        //Pfeile für Regierungsform
        else if (rechts2.getBoundingRectangle().contains(screenX, Gdx.graphics.getHeight()-screenY)) {
            indexRegierung = (indexRegierung + 1) % 4;
        } else if (links2.getBoundingRectangle().contains(screenX, Gdx.graphics.getHeight()-screenY)) {
            indexRegierung = Math.floorMod(indexRegierung - 1, 4); //negativer Modulo, daher Math.floorMod
        }
        //Abfrage für fertig
        else if (fertig.getBoundingRectangle().contains(screenX, Gdx.graphics.getHeight()-screenY)) {
            fertig();
        }

        return true;
    }

    /** Aufruf wenn Maustaste gerade gelößt wurde */
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    /** Aufruf wenn Maustaste gedrückt ist und sich bewegt */
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (auswahl == Auswahl.oberhaupt) {
            if (Auswahl.oberhaupt.getHit(screenX, screenY)) {
                if (werkzeug==Werkzeug.pinsel || werkzeug==Werkzeug.radierer) {
                    int auflösung = 2 + (radius / 10); //höhere Auflösung für höheren Radius
                    float xPix = screenX - Auswahl.oberhaupt.getSprite().getX();
                    float yPix = screenY - Auswahl.oberhaupt.getSprite().getY();
                    drawCircle((int) xPix, (int) yPix, radius);

                    int segments = 1;
                    if (Math.abs(lastX - xPix) > radius / auflösung) {
                        segments = (int) (Math.abs(lastX - xPix) / (radius / auflösung));
                    }
                    if (Math.abs(lastY - yPix) > radius / auflösung && segments < Math.abs(lastY - yPix) / (radius / auflösung)) {
                        segments = (int) (Math.abs(lastY - yPix) / (radius / auflösung));
                    }
                    for (int c = 1; c < segments; c++) {
                        drawCircle((int) (xPix + ((lastX - xPix) / segments * c)), (int) (yPix + ((lastY - yPix) / segments * c)), radius);
                    }
                    lastX = xPix;
                    lastY = yPix;
                }
                else if(werkzeug==Werkzeug.pipette){
                    pickColor(screenX,screenY);
                }

            } else { //reset um bug zu verhindern wenn man beim draggen die hitbox betritt (position wurde dabei nicht resettet)
                lastX = screenX - Auswahl.oberhaupt.getSprite().getX();
                lastY = screenY - Auswahl.oberhaupt.getSprite().getY();
            }
        }
        else if(auswahl==Auswahl.farbe) { //Farbauswahl
            if (Auswahl.farbe.getHit(screenX, screenY)) {
                 pickColor(screenX, screenY);
            }
        }
        else if(auswahl==Auswahl.helligkeit) {//Helligkeitsauswahl
            pickBrightness(screenX);
        }
        return true;
    }

    /** Aufruf wenn Maus bewegt wird */
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    /** Aufruf wenn Mausrad gedreht wird (-1 oder 1 für Richtung) */
    public boolean scrolled(int amount) {

        if(amount == -1){
            if(radius<30) radius++;
        }
        else if(amount == 1){
            if(radius>2) radius--;
        }
        updateVorschau();
        return false;
    }

    //eigene Methoden um Code zu sparen  / übersichtlichkeit

    /** Zeichnet einen Zeichnet einen Kreis in Abhängigkeit des Werkzeugs
     * @param x     xKoordinate des Kreises
     * @param y     yKoordinate des Kreises
     * @param r     Radius des Kreises */
    private void drawCircle(int x, int y, int r) {
        if(werkzeug==Werkzeug.radierer) oberhauptPix.setColor(0,0,0,0);
        else if(werkzeug==Werkzeug.pinsel) oberhauptPix.setColor(farbeGemischt);
        else if(werkzeug==Werkzeug.pipette) return;
        oberhauptPix.fillCircle(x,y,r);
    }

    /** Wählt eine Farbe an einer Stelle des Screens aus (nur im Farbfenster oder auf Oberhaupt)
     * @param screenX   xKoordinate der Maus
     * @param screenY   yKoordinate der Maus */
    private void pickColor(int screenX, int screenY) {

        //Umwandlung in lokale Koordinaten
        int localX = (int) (screenX - auswahl.getSprite().getX());
        int localY = (int) (auswahl.getSprite().getHeight()-Gdx.graphics.getHeight()+auswahl.getSprite().getY()+screenY);

        if(auswahl==Auswahl.farbe) { //Für Farbe
            farbeAuswahl = new Color(farbePix.getPixel(localX,localY));
            farbeGemischt = new Color(farbeAuswahl.r* helligkeit, farbeAuswahl.g* helligkeit, farbeAuswahl.b* helligkeit, 1);
        }
        else if(auswahl==Auswahl.oberhaupt) { //Für Oberhaupt (wenn durchsichtig, dann Hintegrundfarbe)
            farbeGemischt = new Color(oberhauptPix.getPixel(localX,localY));
            if (farbeGemischt.a==0) farbeGemischt = new Color(farbeLand);
        }
        updateVorschau();
    }

    /** Wählt die Helligkeit an einer xKoordinate aus
     * @param screenX   xKoordinate der Maus */
    private void pickBrightness(int screenX) {

        //Umwandlung lokale Koordinaten, Beschränkung
        float xPix = screenX - Auswahl.helligkeit.getSprite().getX()-10*scale;
        if (xPix<0) xPix=0;
        if (xPix>Auswahl.helligkeit.getSprite().getWidth()-20*scale) xPix = Auswahl.helligkeit.getSprite().getWidth()-20*scale;

        //Farbe updaten
        helligkeit =xPix/(Auswahl.helligkeit.getSprite().getWidth()-20*scale);
        farbeGemischt = new Color(farbeAuswahl.r* helligkeit, farbeAuswahl.g* helligkeit, farbeAuswahl.b* helligkeit, 1);
        updateVorschau();
    }

    /** Vorschau für Pinsel updaten mit neuen Werten */
    private void updateVorschau() {

        //Pixmap leeren, Mittelpunkt
        vorschauPix.setColor(new Color(0,0,0,0));
        vorschauPix.fill();
        int midXY = (int) (30*scale);

        if(werkzeug==Werkzeug.radierer) { //Radierer hat schwarzen dünnen Kreis
            vorschauPix.setColor(new Color(1,1,1,1));
            vorschauPix.drawCircle(midXY,midXY,radius);
        }
        else if(werkzeug==Werkzeug.pinsel){ //Pinsel hat ausgefüllten Kreis in Farbe
            vorschauPix.setColor(farbeGemischt);
            vorschauPix.fillCircle(midXY,midXY,radius);
        }
        else if(werkzeug==Werkzeug.pipette) { //Pipette hat Loch in Mitte, um zu sehen welche Farbe ausgewählt wird
            vorschauPix.setColor(farbeGemischt);
            vorschauPix.fillCircle(midXY,midXY,radius);
            vorschauPix.setColor(new Color(0,0,0,0));
            vorschauPix.fillCircle(midXY,midXY,radius/2);
        }
    }

    /** Farbiges Theme des Landes updaten */
    public void updateOverlay(Color c) {

        farbeLand = new Color(c); //Landfarbe updaten

        //gesamtes Overlay füllen
        overlayPix.setColor(new Color(c.r,c.g,c.b,0.333f));
        overlayPix.fill();

        //Hintergrund für Oberhaupt mit höherer Sichtbarkeit
        overlayPix.setColor(new Color(c.r,c.g,c.b,0.8f));
        Sprite temp=Auswahl.oberhaupt.getSprite();
        overlayPix.fillRectangle((int) (temp.getX()),(int) (temp.getY()),(int) (temp.getWidth()),(int) (temp.getHeight()));

        //Flagge Updaten einzelne Streifen, da unterschiedliche Breite
        overlayPix.setColor(c);
        overlayPix.fillRectangle((int) (60*scale),  0,            (int) (1800*scale),(int) (30*scale));
        overlayPix.fillRectangle((int) (110*scale), (int) (30*scale),(int) (1700*scale),(int) (36*scale));
        overlayPix.fillRectangle((int) (160*scale), (int) (66*scale),(int) (1600*scale),(int) (34*scale));
    }

    /** Aufruf, wenn Land fertig erstellt, Schreiben der Variablen in Land.spieler und Verarbeitung der Textur */
    private void fertig() {
        computeTexture(); //Umwandlung zu png
        Land.spieler.setPlayerLinks(game,batch, nameText, farbeLand, PoliticCrash.Regierungsform.values()[indexRegierung]); //Playerlinks setzen
        Gdx.input.setInputProcessor(null); //Input Processor resetten

        //Ingame Screen erstellen und als Hauptscreen setzen
        InGameScreen screen = new InGameScreen(game, batch);
        game.setInGame(screen);
        game.setScreen(screen);
    }

    /** Verarbeitet die Textur in eine png Datei und speichert diese ab */
    private void computeTexture() {

        Pixmap pix = new Pixmap(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), Pixmap.Format.RGBA8888); //Pixmap auf welche alles gezeichnet wird

        //Hintergrund
        hintergrund.getTextureData().prepare();
        pix.drawPixmap(hintergrund.getTextureData().consumePixmap(),0,0);

        pix.drawPixmap(overlayPix, 0,0);

        überschriften=new Texture("Erstellung\\UberschriftenInGame.png"); //Aktive Skills anstatt Name Ingame
        überschriften.getTextureData().prepare();
        pix.drawPixmap(überschriften.getTextureData().consumePixmap(), (int) (127*scale), (int) (173*scale));

        //Beschreibung(aus Framebuffer), Allignment
        //Texte können nicht ohne weiteres auf eine Pixmap gezeichnet werden, weswegen diese aus dem Framebuffer ausgelesen werden
        Sprite sp = Auswahl.beschreibung.getSprite();
        Pixmap temp = ScreenUtils.getFrameBufferPixmap((int) (sp.getX()), (int) (sp.getY()), (int) (sp.getWidth()), (int) (sp.getHeight()));
        pix.drawPixmap(flipPixmap(temp), (int) (sp.getX()), (int) (Gdx.graphics.getHeight()-sp.getY()-sp.getHeight()));

        allignment[indexAllignment].getTextureData().prepare();
        pix.drawPixmap(allignment[indexAllignment].getTextureData().consumePixmap(),(int) (131*scale), (int) (637*scale));

        //Oberhaupt (Canvas)
        pix.drawPixmap(oberhauptPix,(int) (Auswahl.oberhaupt.getSprite().getX()),(int) (Auswahl.oberhaupt.getSprite().getY()));
        Auswahl.oberhaupt.getSprite().getTexture().getTextureData().prepare();
        pix.drawPixmap(Auswahl.oberhaupt.getSprite().getTexture().getTextureData().consumePixmap(),(int) (Auswahl.oberhaupt.getSprite().getX()), (int) (Auswahl.oberhaupt.getSprite().getY()));

        //Flagge mit Text (Framebuffer)
        flagge.getTexture().getTextureData().prepare();
        pix.drawPixmap(flagge.getTexture().getTextureData().consumePixmap(),0, 0);

        temp = ScreenUtils.getFrameBufferPixmap((int) (flagge.getX()+200*scale), (int) flagge.getY(), (int) (flagge.getWidth()-400*scale), (int) flagge.getHeight());
        pix.drawPixmap(flipPixmap(temp), (int) (flagge.getX()+200*scale), (int) (Gdx.graphics.getHeight()-flagge.getY()-flagge.getHeight()));

        PixmapIO.writePNG(Gdx.files.local("core\\assets\\InGame\\Land\\Spielerland.png"), pix);
    }

    /** notwendig, da Framebuffer invertiert ist, leicht angepasst
     *  Quelle: https://stackoverflow.com/questions/12548532/how-to-flip-a-pixmap-to-draw-to-a-texture-in-libgdx */
    private Pixmap flipPixmap(Pixmap src) {
        final int width = src.getWidth();
        final int height = src.getHeight();
        Pixmap flipped = new Pixmap(width, height, src.getFormat());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                flipped.drawPixel(x, y, src.getPixel(x, height - y - 1));
            }
        }
        return flipped;
    }
}
