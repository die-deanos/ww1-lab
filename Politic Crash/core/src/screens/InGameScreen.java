package screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.PoliticCrash;
import minispiele.MiniGame1;
import minispiele.MiniGame2;
import minispiele.MiniGame3;
import minispiele.MiniGame4;
import objects.*;

public class InGameScreen implements Screen {

	PoliticCrash game; //Spielvariable, zum ändern und Speichern der Screens
	SpriteBatch batch; //Spritebatch, zum Zeichnen alles Grafiken auf dem Bildschirm
	float scale = Gdx.graphics.getWidth()/1920f; //für Bildschirme mit einer anderen Auflösung als FullHD

	//Grafikvariablen
	private Texture landkarte, lines, fenster, aktiveSkills, ausklapp1, ausklapp2; //nicht interagierbar
	private Sprite skill, politik, info, nachrichten, rundeBeenden; //buttons
	private Pixmap farbkarte; //für Landauswahlsystem
	private BitmapFont fontLandInfo, fontNachrichten; //Zeichnen von Text

	private Land auswahl; //ausgewähltes Land
	private boolean ausgeklapt; //Nachrichtenfenster ausgeklappt
	private boolean gewonnen, verlohren; //Ob winning oder loosing bedingung erfüllt ist

	//Skillbaum Screens
	private SkillbaumScreen skillbaumScreen;
	private PolitikbaumScreen politikbaumScreen;

	//Zählvariablen
	private int runde = 1;
	private int amZug = 0;
	private int timer = 0;

	//Informationen Zeichnen (für Länderscreen und Skilltrees)
	GlyphLayout layout;
	BitmapFont font44 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_44.fnt"));
	BitmapFont font32 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_32.fnt"));
	Texture infoName = new Texture("InGame\\Skilltree\\InfoName.png");
	Texture infoKosten = new Texture("InGame\\Skilltree\\InfoKosten.png");
	Texture infoBeschreibung = new Texture("InGame\\Skilltree\\InfoBeschreibung.png");

	/** Konstruktor */
	public InGameScreen(PoliticCrash game, SpriteBatch batch) { //Konstruktor
		this.game = game;
		this.batch = batch;

		//Nachrichtensystem starten
		Nachrichtsystem.setLinks(this);
		Nachrichtsystem.einlesenQuatsch();
		Nachrichtsystem.spezialNachricht("In diesem Spiel dreht sich alles um die Datensammlung, du wirst keinerlei Informationen über die Ausgänge deiner Aktionen erhalten, bis du nicht die dafür nötigen Skills freischaltest.");
		Nachrichtsystem.spezialNachricht("Willkommen bei Politic Crash, klappe mich auf für mehr Informationen!");
		ausgeklapt = false;

		//Laender verlinken
		for (Land l : Land.values()) {
			l.setBotLinks(game, batch);
			l.vertrauenAendern(60,l); //jedes Land hat zu sich selbst mehr Vertrauen
		}

		//SkilltreeScreen inizialisieren
		skillbaumScreen = new SkillbaumScreen(game, batch);
		politikbaumScreen = new PolitikbaumScreen(game, batch);

	}

	/** LibGdx interner Aufruf wenn Screen zum aktiven Screen wird */
	public void show() {

		//Texturen
		landkarte = new Texture("InGame\\Main\\Landkarte.png");
		lines = new Texture("InGame\\Main\\Lines.png");
		fenster = new Texture("InGame\\Main\\Window.png");
		aktiveSkills = new Texture("InGame\\Main\\Aktive Skills\\AktiveSkills.png");
		ausklapp1 = new Texture("InGame\\Main\\NachichtenAusklappbar1.png");
		ausklapp2 = new Texture("InGame\\Main\\NachichtenAusklappbar2.png");

		//Sprites
		skill = new Sprite(new Texture("InGame/Main/Skills.png"));
		skill.setSize(200*scale, 200*scale);
		skill.setPosition(14*scale,Gdx.graphics.getHeight()-214*scale);

		politik = new Sprite(new Texture("InGame/Main/Politik.png"));
		politik.setSize(200*scale,200*scale);
		politik.setPosition(14*scale,Gdx.graphics.getHeight()-428*scale);

		info = new Sprite(new Texture("InGame/Main/Info.png"));
		info.setSize(200*scale, 200*scale);
		info.setPosition(1704*scale,Gdx.graphics.getHeight()-214*scale);

		nachrichten = new Sprite(new Texture("InGame/Main/Nachrichten.png"));
		nachrichten.setSize(1400*scale, 100*scale);
		nachrichten.setPosition(260*scale,Gdx.graphics.getHeight()-114*scale);

		rundeBeenden = new Sprite(new Texture("InGame/Main/Runde.png"));
		rundeBeenden.setSize(Gdx.graphics.getWidth(), 100*scale);
		rundeBeenden.setPosition(0,0);

		//Colorpixmap
		Texture f = new Texture("InGame/Main/Farbkarte.png");
		f.getTextureData().prepare();
		farbkarte = f.getTextureData().consumePixmap();

		//Textrenderer
		font32 = new BitmapFont(Gdx.files.internal("InGame\\Font\\EngraversGothicBT_32.fnt"));

		fontLandInfo = new BitmapFont(Gdx.files.internal("InGame\\Font\\LandInfo.fnt"));
		fontNachrichten = new BitmapFont(Gdx.files.internal("InGame\\Font\\Nachrichten.fnt"));

		timer=0; //timer reset um button zu disablen
	}

	/** LibGdx interner Aufruf zum rendern des Screens */
	public void render(float delta) {
		int w=Gdx.graphics.getWidth(), h=Gdx.graphics.getHeight();

		//Klickabfrage
		timer++;
		if(Land.values()[amZug].getErobertVon(Land.spieler)) { //erobertes Land am Zug
			if(Gdx.input.justTouched()) { //wenn geklickt wurde
				if(auswahl!=null && info.getBoundingRectangle().contains(Gdx.input.getX(), h-Gdx.input.getY())) { //wenn die maus über dem infobutton ist + ein Land ausgewählt ist
					game.setScreen(auswahl.getScreen()); //screenwechsel zum entsprechenden Land
				}
				else if(skill.getBoundingRectangle().contains(Gdx.input.getX(), h-Gdx.input.getY()) && !getZuEnde()) { //wenn die maus über dem skillbutton ist und Spiel am laufen
					game.setScreen(skillbaumScreen); //screenwechsel zum Skillbaum
				}
				else if(politik.getBoundingRectangle().contains(Gdx.input.getX(), h-Gdx.input.getY()) && !getZuEnde()) { //wenn die maus über dem politikbutton ist und Spiel am laufen
					game.setScreen(politikbaumScreen); //screenwechsel zum Politikbaum
				}
				else if(timer>20 && rundeBeenden.getBoundingRectangle().contains(Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY())) { //wenn die maus über dem rundebeenden button ist und nicht allererster Frame
					if(getZuEnde()) {
						game.dispose();
						Gdx.app.exit();
					}
					else {
						auswahl=null;
						zugBeenden();
					}
				}
				else { //Wenn kein Button ausgewählt wurde, Landauswahlsystem
					auswahl=landBeiPunkt(Gdx.input.getX(), Gdx.input.getY());
				}
			}

		}
		else { //Zug PC
			if (timer==60) {

				Land.values()[amZug].getBot().botZug();

				zugBeenden();
			}
				
		}

		//Buttons die unabhängig des Zuges immer benutzbar sind
		if(Gdx.input.justTouched()) {
			if(nachrichten.getBoundingRectangle().contains(Gdx.input.getX(), h-Gdx.input.getY())) { //wenn die maus über dem politikbutton ist
				if(ausgeklapt) ausgeklapt=false;
				else ausgeklapt=true;
			}
		}
		if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) { //Zurück zum Hauptmenu
			game.setScreen(game.getMainMenu());
		}

		//Cheatcodes zum Testen
		if(Gdx.input.isKeyJustPressed(Input.Keys.F1)) {
			Land l = landBeiPunkt(Gdx.input.getX(), Gdx.input.getY());
			if (l!=null) {
				l.erobern(Land.spieler);
			}
		}
		if(Gdx.input.isKeyJustPressed(Input.Keys.F2)) {
			Land l = landBeiPunkt(Gdx.input.getX(), Gdx.input.getY());
			if (l!=null) {
				l.informationenAendern(100,l);
			}
		}


		//Zeichnen
		batch.begin();//Start des Zeichenprozesses
		batch.draw(landkarte, 0, 0, w, h); //Hintergrund


		//Overlay für Auswahl
		if(auswahl!=null) { //Wenn ein Land ausgewählt ist
			batch.setColor(auswahl.getErobertVon().getFarbe());
			batch.draw(auswahl.getOverlay(), 0, 0, w, h);
		}

		//Overlay für AmZug
		if(!getZuEnde()) { //solange Spiel am Laufen
			Color c = Land.values()[amZug].getErobertVon().getFarbe();
			batch.setColor(c.r, c.g, c.b, 0.4f);
			batch.draw(Land.values()[amZug].getOverlay(), 0, 0, w, h);
			batch.setColor(1, 1, 1, 1f);
		}

		batch.draw(lines,0, 0, w, h); //Linien, extra, da overlay darunter

		drawLandInfo(); //Fenster mit Informationen zum Land
		drawAktiveSkills();
		drawNachrichten(); //Nachrichtenfenster

		//Buttons
		skill.draw(batch);
		politik.draw(batch);
		info.draw(batch);
		rundeBeenden.draw(batch);

		batch.end();
	}

	/** Ungenutzt da fullscreen */
	public void resize(int width, int height) {

	}

	public void pause() {

	}

	public void resume() {

	}

	public void hide() {

	}

	/** Spezielles entfernen für Texturen aus dem Grafikspeicher */
	public void dispose() {
		//Screens disposen
		skillbaumScreen.dispose();
		politikbaumScreen.dispose();
		for(Land l : Land.values()) {
			l.getScreen().dispose();
		}

		//Texturen disposen
		landkarte.dispose();
		lines.dispose();
		fenster.dispose();
		ausklapp1.dispose();
		ausklapp2.dispose();

		//Sprites disposen
		skill.getTexture().dispose();
		politik.getTexture().dispose();
		info.getTexture().dispose();
		nachrichten.getTexture().dispose();
		rundeBeenden.getTexture().dispose();

		//Textrenderer disposen
		font32.dispose();
		fontLandInfo.dispose();
	}

	/**	Findet das Land an einem bestimmten Punkt mithilfe der Colormap
	 * @param x 	xKoordinate des Cursors
	 * @param y 	yKoordinate des Cursors
	 * @return		Land an der Stelle des Cursor */
	private Land landBeiPunkt(int x, int y) {
		Land land=null;
		Color c = new Color(farbkarte.getPixel(x, y)); //ueberpruefen der Colormap an stelle des Cursors
		for (Land l : Land.values()) { //fuer alle Laender
			if (!l.equals(Land.spieler)) {
				if (c.equals(l.getFarbe())) land = l; //wenn Farbe identisch mit der des Landes, auswaehlen
			}
			else if (c.equals(new Color(1,1,1,1))) land = l; //Sonderfall für Sielerland
		}
		if (c.equals(new Color(0, 0, 0, 0))) land = null; //Sonderfall deselect wenn Weis
		return land;
	}

	/**	Aufruf wenn Runde beendet, triggert Minispiele, testet auf Sieg/Niederlage und ruft Skills auf */
	private void zugBeenden() {
		timer=0; //timer resetten
		amZug++; //Zeiger inkrementieren
		if (amZug==8) {
			amZug=0;
			runde++;

			//Minispiel auslösen wenn Spieler gerade am Zug
			if(Math.random()*10<4) { //40% Chance auf Minispiele
				int type = (int) (Math.random()*4); //Zahlen von 0 bis 3
				ScreenAdapter minigame=null;
				if (type==0) minigame = new MiniGame1(game, batch);
				else if (type==1) minigame = new MiniGame2(game, batch);
				else if (type==2) minigame = new MiniGame3(game, batch);
				else if (type==3) minigame = new MiniGame4(game, batch);
				game.setScreen(minigame);
			}

			Nachrichtsystem.spassNachricht(); //Spaßnachricht feuern (chancenbedingt)
		}

		//Test der Winning und Loosing Conditions
		verlohren=true;
		for (Land l : Land.values()) {
			if (l.getErobertVon(Land.spieler)) { //Wenn es ein Land gibt welches erobert ist geht das Spiel weiter
				verlohren= false;
				break;
			}
		}
		if (verlohren) Nachrichtsystem.spezialNachricht("Dein Land wurde erobert und du konntest nicht fliehen. Du hast verloren!");

		gewonnen=true;
		for (Land l : Land.values()) {
			if (!l.getErobertVon(Land.spieler)) { //Wenn es ein Land gibt welches nicht erobert ist, geht das Spiel weiter
				gewonnen= false;
				break;
			}
		}
		if(gewonnen) Nachrichtsystem.spezialNachricht("Alle Länder wurden Erobert. Du hast gewonnen!");

		if(getZuEnde()) {
			rundeBeenden = new Sprite(new Texture("InGame/Main/SpielBeenden.png"));
			rundeBeenden.setSize(Gdx.graphics.getWidth(), 100*scale);
			rundeBeenden.setPosition(0,0);
		}

		//Beginn der nächsten runde
		auswahl=Land.values()[amZug];
		Land.values()[amZug].skillAufruf();//Alle skills welche momentan in Aktion sind aufrufen
	}

	/** Getter für amZug */
	public int getAmZug() {
		return amZug;
	}

	/** Getter für SpielZuEnde */
	public boolean getZuEnde() {
		if(gewonnen || verlohren) return true;
		else return false;
	}

	/**	Zeichnet das Nachrichtenfenster abhängig des Zustandes */
	public void drawNachrichten() {
		int nachrichtenBreite = (int) (1100*scale);//wie breit ein String sein darf bevor er geteilt wird
		int maxZeilen = 11; //Anzahl
		int index=0; //aktuelle nachricht
		boolean einZeiler=true; //Ob oberstes Feld 1 oder 2 Zeilen beinhaltet

		//Nachrichten mit entsprechender Schriftart in eine Liste umgewandelt
		ArrayList<String> strings = new ArrayList<>();
		ArrayList<Color> colors = new ArrayList<>();

		nachrichten.draw(batch); //Nachrichtenfenstersprite zeichnen

		if (Nachrichtsystem.getSize()>0) { //mindestens eine Nachricht

			//Umwandlung zu Zeilen mit richtiger Länge
			//Hauptnachricht
			Nachricht n = Nachrichtsystem.getNachricht(index);
			ArrayList<String> einzelnSplit = splitString(n.getInhalt(),nachrichtenBreite, fontNachrichten);
			if (einzelnSplit.size()>1) {
				einZeiler = false;
				maxZeilen++;
			}
			for(String s : einzelnSplit) {
				strings.add(s);
				colors.add(n.getColor());
			}
			index++;
			//restliche Nachrichten
			while(strings.size()<maxZeilen && index<Nachrichtsystem.getSize()) {
				n = Nachrichtsystem.getNachricht(index);
				einzelnSplit = splitString(n.getInhalt(),nachrichtenBreite, fontNachrichten);
				for(String s : einzelnSplit) {
					strings.add(s);
					colors.add(n.getColor());
				}
				index++;
			}

			//Anzeigen
			String string;
			BitmapFont font;
			index=0;
			int y;

			//Oberstes Feld
			if(!einZeiler) {
				y= (int) (Gdx.graphics.getHeight()-39*scale);
				for(; index<2; index++) {
					string=strings.get(index);
					layout = new GlyphLayout(fontNachrichten, string);
					fontNachrichten.setColor(colors.get(index));
					fontNachrichten.draw(batch,string,(Gdx.graphics.getWidth()-layout.width)/2, y+layout.height/2);
					y-=50*scale;
				}
			}
			else {
				y= (int) (Gdx.graphics.getHeight()-64*scale); //Mitte des Nachrichten Sprites
				string=strings.get(index);
				layout = new GlyphLayout(fontNachrichten, string);
				fontNachrichten.setColor(colors.get(index));
				fontNachrichten.draw(batch,string,(Gdx.graphics.getWidth()-layout.width)/2, y+layout.height/2);
				index++;
				y-=75*scale;
			}
			//Ausklappbarer Teil
			if (strings.size()>1 && ausgeklapt) {//ausklappbar und ausgeklappt
				for(; index<strings.size(); index++) {
					string=strings.get(index);

					Texture t; //für letztes Abschlusstextur verwenden
					if (index==strings.size()-1) t = ausklapp2;
					else t=ausklapp1;
					batch.draw(t, 360*scale, y-25*scale, 1200*scale, 50*scale);

					layout = new GlyphLayout(fontNachrichten, string);
					fontNachrichten.setColor(colors.get(index));
					fontNachrichten.draw(batch,string,(Gdx.graphics.getWidth()-layout.width)/2, y+layout.height/2);
					y-=50*scale;
				}
			}
		}
	}

	/**	Zeichnet das Informationsfenster für die aktuelle Auswahl */
	public void drawLandInfo() {
		int x= (int) (info.getX()+info.getWidth()-300*scale);
		int y= (int) (info.getY()-212*scale);
		batch.draw(fenster, x, y, 300*scale, 198*scale);
		String s0 = "Keine Auswahl"; String s1 = ""; String s2 = "";
		if(auswahl!=null) {
			s0 = auswahl.getName();
			s1+= Land.spieler.getVertrauen(auswahl);
			s2+= Land.spieler.getInformationen(auswahl);
		}
		font32.draw(batch, s0, x+26*scale,y+175*scale);
		fontLandInfo.setColor(133f/255f,217f/255f,0,1);
		fontLandInfo.draw(batch, s1,x+26*scale,y+105*scale);
		fontLandInfo.setColor(20f/255f,191f/255f,1,1);
		fontLandInfo.draw(batch, s2,x+26*scale,y+50*scale);
	}

	public void drawAktiveSkills() {
		int x = 1764;
		int y = 372;

		batch.draw(aktiveSkills,x,y,140,267);
		if(Land.values()[amZug].getErobertVon(Land.spieler) && !getZuEnde()) { //nicht mehr Zeichnen wenn Spiel zu ende
			Skill[] skills = Land.values()[amZug].getAktionen();

			for(int i=0; i<skills.length;i++) {
				if(skills[i]!=null) {
					Texture t = skills[i].getSprites()[skills[i].getLvl() - 1].getTexture();
					if (t.getWidth() != t.getHeight()){ //Sonderfall Doppelskills
						try {//Erstellt Sprite abhängig von Name
							t = new Texture("InGame\\Main\\Aktive Skills\\" + skills[i].getName() + ".png");
						} catch (Exception e) { //Sondefall wenn nicht vorhanden
							t = new Texture("InGame\\Skilltree\\Symbole\\Placeholder1.png");
						}
					}
					batch.draw(t, x + 15, y + 137-116*i, 109, 109);
				}
				else break;
			}
		}
	}

	/** Zeichnet das Informationsfenster eines spezifischen Skills
	 * @param skill		Skill dessen Informationen gezeichnet werden
	 * @param level		Level des Skills
	 * @param details	Flag für Details (im Skill und Politikscreen) */
	public void drawSkillInfo(Skill skill, int level, boolean details) {
		//Werte berechnen
		int x = Gdx.input.getX();
		int y = Gdx.graphics.getHeight() - Gdx.input.getY();
		int w = (int) (400*scale);

		//Optionen
		boolean kosten=false, risiko=false, warten=false;
		String kostenStr="Kosten: ", risikoStr="Risiko: ", warteStr = "";

		//welche Optionen werden gebraucht
		if(details) {
			kosten=true;
			if (level >= skill.getLvl()) kostenStr+=skill.getKosten();
			else kostenStr+="-";
		}
		else {
			Aktiv a = (Aktiv) skill;
			if(a.getAnwendungsKosten()>0) {
				kosten=true;
				kostenStr+=a.getAnwendungsKosten();
			}
			if(a.getRisiko()!=0) {
				risiko=true;
				risikoStr+=a.getRisiko();
			}
			if(a.getMaxRunde()!=0) {
				warten=true;
				warteStr="Wartezeit: " + a.getMaxRunde();
			}
			if(a.getDauer()!=0) {
				warten=true;
				warteStr="Dauer: " + a.getDauer();
			}
			if(warten) {
				if(a.getMaxRunde()>1 || a.getDauer()>1) warteStr += " Runden";
				else warteStr += " Runde";
			}
		}

		//Optionen einzeln Zeichnen
		if (details) { //Für Skillbaum (alle Details)
			String nameStr="Name: "+skill.getName(); //Name holen
			layout = new GlyphLayout(font44, nameStr); //Pixelweite berechnen
			if(layout.width+20*scale>w) w = (int) (layout.width+20*scale); //Mindestbreite
			if(x>1080*scale) x-=w; //Verschieben wenn auf rechter Seite

			batch.draw(infoName,x,y-55*scale,w,55*scale);
			font44.draw(batch, nameStr, x+10*scale, y-13*scale);
			y -= 55*scale;
		}

		//Kosten
		if(kosten) {
			batch.draw(infoKosten,x,y-38*scale,w,38*scale);
			font32.draw(batch, kostenStr, x+10*scale, y-8*scale);
			y -= 38*scale;
		}

		//Risiko
		if(risiko) {
			batch.draw(infoKosten,x,y-38*scale,w,38*scale);
			font32.draw(batch, risikoStr, x+10*scale, y-8*scale);
			y -= 38*scale;
		}

		//Dauer / Aktivierungszeit
		if(warten) {
			batch.draw(infoKosten,x,y-38*scale,w,38*scale);
			font32.draw(batch, warteStr, x+10*scale, y-8*scale);
			y -= 38*scale;
		}

		//Beschreibunglabel
		if(details || risiko || kosten) {
			batch.draw(infoBeschreibung,x,y-38*scale, w,38*scale);
			font32.draw(batch, "Beschreibung:", x+10*scale, y-10*scale);
			y -= 38*scale;
		}

		//Beschreibung
		for (String str : splitString(skill.getBeschreibung(), (int) (w-40*scale), font32)) {
			batch.draw(infoBeschreibung,x,y-38*scale,w,38*scale);
			if (details) str = " " + str;
			font32.draw(batch, str, x+10*scale, y-10*scale);
			y -= 38*scale;
		}
	}


	/** Teilt einen Vorgegebenen String in mehrere Zeilen
	 * @param s		zu teilender String
	 * @param width	maximale Breite eines Strings in Pixeln
	 * @param font	Schriftart und Schriftgröße
	 * @return 		Liste der Erstellten Strings */
	private ArrayList<String> splitString(String s, int width, BitmapFont font) {
		ArrayList<String> split = new ArrayList<String>();
		String tempStr = "";
		for (String str : s.split(" ")) { //String unterteilen in Wörterarray (Trennen bei Leerzeichen)
			layout = new GlyphLayout(font, tempStr + str);
			if (layout.width > width) { //wenn neuer String zu lang
				split.add(tempStr.substring(0, tempStr.length()-1)); //Hinzufügen ohne letztes Leerzeichen
				tempStr = "";
			}
			tempStr += str + " "; //neues Wort anhängen
		}
		split.add(tempStr.substring(0, tempStr.length()-1));

		return split;
	}

}
