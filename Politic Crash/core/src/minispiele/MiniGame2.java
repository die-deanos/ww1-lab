package minispiele;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.PoliticCrash;
import objects.Land;
import objects.Nachrichtsystem;

import java.util.Random;

public class MiniGame2 extends ScreenAdapter {

    private PoliticCrash game;
    private SpriteBatch batch;

    private Texture hintergrund;
    private ShapeRenderer shapeRenderer;
    private Vector2 posBase;
    private Vector2 bewBlock;
    private Vector2[] posBlock = new Vector2[5];
    private int breite;
    private int positionArray[]; //wo die blöcke sind
    private Random rnd;
    private int blocknr=0; //welcher block gerade dran ist
    private int treffer=0; //wie viele Blöcke getroffen wurden.

    private BitmapFont schrift =new BitmapFont();
    private float[] posSchrift = new float[5];

    public MiniGame2(PoliticCrash game, SpriteBatch batch) {
        this.game = game;
        this.batch = batch;

        rnd=new Random();
        shapeRenderer = new ShapeRenderer();
        breite = Gdx.graphics.getWidth();
        positionArray = new int[5];
        posBase = new Vector2(10, 20);
        bewBlock = new Vector2(0,-100);
        hintergrund= new Texture("InGame/Minispiel/Layout_Landkarte_dunkel.png");

        for(int i=0; i<positionArray.length;i++){
            positionArray[i]=(rnd.nextInt(5)+1);
        }
        int y=Gdx.graphics.getHeight();
        int x=0;
        int teilung=breite/5;
        for (int i=0;i<positionArray.length;i++) {
            switch (positionArray[i]) {
                case 1:
                    x = teilung * 1 - breite / 10 - 25;
                    break;
                case 2:
                    x = teilung * 2 - breite / 10 - 25;
                    break;
                case 3:
                    x = teilung * 3 - breite / 10 - 25;
                    break;
                case 4:
                    x = teilung * 4 - breite / 10 - 25;
                    break;
                case 5:
                    x = teilung * 5 - breite / 10 - 25;
                    break;
            }
            posBlock[i]= new Vector2(x,y);
            y=y+100;
        }
        for (int i=0;i<5;i++) {
            posSchrift[i]= (teilung * (i+1) - breite / 10 - 25)+10;
        }
    }

    public boolean zwischen(float x,float unten, float oben){
        return unten <= x && x <= oben;
    }

    @Override
    public void render(float delta) {

        int w=Gdx.graphics.getWidth(); int h=Gdx.graphics.getHeight();
        batch.begin();
        batch.draw(hintergrund, 0, 0, w, h);
        batch.end();
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)){
            if ((positionArray[blocknr]==1)&&(zwischen(posBlock[blocknr].y,posBase.y,posBase.y+50))){
                treffer++;
                posBlock[blocknr].x=-100;
            }
            blocknr++;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.F)){
            if ((positionArray[blocknr]==2)&&(zwischen(posBlock[blocknr].y,posBase.y,posBase.y+50))){
                treffer++;
                posBlock[blocknr].x=-100;
            }
            blocknr++;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            if ((positionArray[blocknr]==3)&&(zwischen(posBlock[blocknr].y,posBase.y,posBase.y+50))){
                treffer++;
                posBlock[blocknr].x=-100;
            }
            blocknr++;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.J)){
            if ((positionArray[blocknr]==4)&&(zwischen(posBlock[blocknr].y,posBase.y,posBase.y+50))){
                treffer++;
                posBlock[blocknr].x=-100;
            }
            blocknr++;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.K)){
            if ((positionArray[blocknr]==5)&&(zwischen(posBlock[blocknr].y,posBase.y,posBase.y+50))){
                treffer++;
                posBlock[blocknr].x=-100;
            }
            blocknr++;
        }
        if (blocknr>=5){
            Land l = Land.values()[(int)(Math.random()*7)]; //Zufälliges Land
            Land.spieler.informationenAendern(5*treffer,l); //Informationen hinzufügen
            //Nachricht feuern
            if(treffer>0) Nachrichtsystem.spezialNachricht("Du konntest "+5*treffer+" Informationen von "+l.getName()+" sicherstellen.");
            else Nachrichtsystem.spezialNachricht("Du konntest keinerlei Informationen gewinnen.");
            game.setScreen(game.getInGame()); //Rückkehr
        }

        //bewegung der Blöcke
        for (int i=0;i<posBlock.length;i++) {
            posBlock[i].mulAdd(bewBlock, delta);
        }

        //Checkbox
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.LIME);
        shapeRenderer.rect(posBase.x,posBase.y,(breite-20),50);
        shapeRenderer.end();

        //blöcke zeichnen
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        for(int i=0;i<positionArray.length;i++){
           shapeRenderer.rect(posBlock[i].x,posBlock[i].y,50,50);
        }
        shapeRenderer.end();

        //Beschriftung zeichnen
        batch.begin();
        schrift.draw(batch,"D" ,posSchrift[0] , posBase.y+25);
        schrift.draw(batch,"F" ,posSchrift[1] , posBase.y+25);
        schrift.draw(batch,"Leer",posSchrift[2] , posBase.y+25);
        schrift.draw(batch,"J" ,posSchrift[3] , posBase.y+25);
        schrift.draw(batch,"K",posSchrift[4] , posBase.y+25);
        batch.end();

    }


    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.dispose();
    }
}
