package minispiele;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.PoliticCrash;
import objects.Land;
import objects.Nachrichtsystem;


public class MiniGame1 extends ScreenAdapter {

    private PoliticCrash game;
    private SpriteBatch batch;

    private Texture hintergrund;
    private ShapeRenderer shapeRenderer;
    private Vector2 posBase;//Position des Basis Quadratss
    private Vector2 posScan; //Position des Scanners
    private Vector2 bewScan; //Bewegung des Chans
    private Vector2 posZiel; //Position Des Ziel
    private int halbehoehe;
    private int breite;
    private int runde; //Var für mehr als 1 Runde
    private int treffer;
    private int zielStart;


    public MiniGame1(PoliticCrash game, SpriteBatch batch) {
        this.game = game;
        this.batch = batch;

        shapeRenderer = new ShapeRenderer();
        halbehoehe = Gdx.graphics.getHeight()/2;
        breite = Gdx.graphics.getWidth();
        posBase = new Vector2(10,halbehoehe-50);
        posScan = new Vector2(breite/2-10,halbehoehe-40);
        zielStart =(int)(Math.random()*((breite-130)-30+1)+30);
        posZiel = new Vector2(zielStart,halbehoehe-45);
        bewScan = new Vector2(300,0);
        runde = 0;
        treffer = 0;
        hintergrund= new Texture("InGame/Minispiel/Layout_Landkarte_dunkel.png");
    }
    //bildwiedergabe
    public void spielfeld(){
        int w=Gdx.graphics.getWidth(); int h=Gdx.graphics.getHeight();
        batch.begin();
        batch.draw(hintergrund, 0, 0, w, h);
        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(20f/255f,191f/255f,255f/255f,1);
        shapeRenderer.rect(posBase.x,posBase.y,(breite-20),100);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(166f/255f,255f/255f,166f/255f,1);
        shapeRenderer.rect(posZiel.x,posZiel.y,100,90);
        shapeRenderer.setColor(20f/255f,191f/255f,255f/255f,1);
        shapeRenderer.rect(posScan.x,posScan.y, 20,80);
        shapeRenderer.end();
    }

    @Override
    public void render(float delta) {

        //anstoß Links
        if(posScan.x <=30){
            posScan.x =30;
            bewScan.x = -bewScan.x;
        }
        //anstoß rechts
        if(posScan.x >=breite-30){
            posScan.x =breite-30;
            bewScan.x = -bewScan.x;
        }

        //Zielbedingung 5 Runden
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            if((posScan.x <= (posZiel.x + 100))&&((posScan.x + 20) >= posZiel.x)){
                treffer=treffer+1;

            }else{

            }
            runde=runde+1;
            zielStart =(int)(Math.random()*((breite-130)-30+1)+30);
            posZiel.set(zielStart,halbehoehe-45);
        }
        //bedingung für minispielbeenden
        if(runde>=5){
            Land l = Land.values()[(int)(Math.random()*7)]; //Zufälliges Land
            Land.spieler.informationenAendern(5*treffer,l); //Informationen hinzufügen
            //Nachricht feuern
            if(treffer>0) Nachrichtsystem.spezialNachricht("Du konntest "+5*treffer+" Informationen von "+l.getName()+" sicherstellen.");
            else Nachrichtsystem.spezialNachricht("Du konntest keinerlei Informationen gewinnen.");
            game.setScreen(game.getInGame()); //Rückkehr
        }
        //bewegung
        posScan.mulAdd(bewScan, delta);

        spielfeld();

    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        this.dispose();
    }
}
