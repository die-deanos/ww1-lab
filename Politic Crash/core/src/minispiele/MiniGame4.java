package minispiele;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.PoliticCrash;
import objects.Land;
import objects.Nachrichtsystem;

import java.util.Arrays;
import java.util.Random;

public class MiniGame4 extends ScreenAdapter {
    private PoliticCrash game;
    private SpriteBatch batch;

    private Texture hintergrund;
    private char vorgabe[];
    private char randBS[][]; //2-Dim Array wo buchstaben für die Runden drinne sind
    private char eingabe[];
    private int runde;
    private int h; //Fenster hoehe
    private int w;    // fersterbreite
    private ShapeRenderer sr;
    private BitmapFont schrift;
    private int screenfase;

    public MiniGame4(PoliticCrash game, SpriteBatch batch) {
        this.game = game;
        this.batch = batch;

        // inizialisieren
        hintergrund= new Texture("InGame/Minispiel/Layout_Landkarte_minigame4.png");
        sr= new ShapeRenderer();
        vorgabe = new char[5];
        randBS = new char[5][4];
        eingabe = new char[5];
        Random rnd= new Random();
        batch = new SpriteBatch();
        schrift = new BitmapFont();
        schrift.setColor(Color.RED);
        runde=0;
        h =Gdx.graphics.getHeight();
        w = Gdx.graphics.getWidth();
        screenfase =0;

        //Random Merkfolge
        for (int i = 0; i < vorgabe.length; i++) {
            vorgabe[i]= (char)(rnd.nextInt(26) + 'A');
        }

        //System.out.println(vorgabe);
        //vorbereitung
        for (int i = 0; i < randBS.length; i++) {
            randBS[i][rnd.nextInt(4)]=vorgabe[i];   //Zufälliges übergeben der Merkfolge an feldplatz 0-3
            for (int j = 0; j < randBS[i].length; j++) {
                if (randBS[i][j] != vorgabe[i]) {       //Prüfen ob bereist Merkfolge an platz ist Wenn nicht Randam Buchstabe generieren
                    do {
                        randBS[i][j] = (char) (rnd.nextInt(26) + 'A');
                    } while(bsdopplung(i,j,randBS[i][j])); //Verhindern von Buchstabendopplung innerhalb einer sequenz
                }
            }
        }
    }

    public  boolean bsdopplung(int i,int j, char c){
        boolean gleich = false;
        for (int k = 0; k < j; k++) {
            if(randBS[i][k]== c){
                gleich=true;
            }
        }
        return gleich;
    }

    //prozedur für das Grundspielfeld
    public void spielfeld(){

        batch.begin();
        batch.draw(hintergrund, 0, 0, w, h);
        batch.end();
    }

    //Ausgelagerte Prozedur um Buchstagben anzuzeigen
    public void buchstaben(int r){

        batch.begin();
        schrift.draw(batch, String.valueOf(randBS[r][0]), w/2-h/4, h/2+h/4);
        schrift.draw(batch, String.valueOf(randBS[r][1]), w/2+h/4, h/2+h/4);
        schrift.draw(batch, String.valueOf(randBS[r][2]), w/2-h/4, h/2-h/4);
        schrift.draw(batch, String.valueOf(randBS[r][3]), w/2+h/4, h/2-h/4);
        batch.end();
    }

    public char wahl(int x, int y,int r){
        char c;
        if(x<= w /2){
            if (y<= h /2){
                c=randBS[r][0];
            }else{
                c=randBS[r][2];
            }
        }else {
            if (y<= h /2){
                c=randBS[r][1];
            }else{
                c=randBS[r][3];
            }
        }
        return c;
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            screenfase = 1;
        }
        spielfeld();
        switch (screenfase) {
            case 0:
                batch.begin();
                schrift.draw(batch, String.valueOf(vorgabe), w / 2, h / 2);
                schrift.draw(batch, "Los mit Leertaste", w / 2, h / 2 - 50);
                batch.end();
                break;
            default:
                if (runde<vorgabe.length) {
                    buchstaben(runde);
                    if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
                        int x = Gdx.input.getX();
                        int y = Gdx.input.getY();
                        eingabe[runde] = wahl(x, y, runde);
                        runde++;
                    }
                    if (runde == vorgabe.length) {
                        if (Arrays.equals(vorgabe, eingabe)) {
                            Land l = Land.values()[(int)(Math.random()*7)]; //Zufälliges Land
                            Land.spieler.informationenAendern(25,l); //Informationen hinzufügen
                            Nachrichtsystem.spezialNachricht("Du konntest 25 Informationen von "+l.getName()+" sicherstellen.");
                        }
                        else Nachrichtsystem.spezialNachricht("Du konntest keinerlei Informationen gewinnen.");
                        game.setScreen(game.getInGame()); //Rückkehr
                    }
                }
                break;

        }
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.dispose();
    }
}
