package minispiele;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.PoliticCrash;
import objects.Land;
import objects.Nachrichtsystem;

import java.util.Arrays;
import java.util.Random;

public class MiniGame3 extends ScreenAdapter {
    private PoliticCrash game;
    private SpriteBatch batch;

    private Texture hintergrund;
    private ShapeRenderer sr;
    private int figur[][];
    private int bild[][];
    private float linen[][];
    private Random rnd;
    private Vector2 tabEnde;
    private int screenfase;
    private BitmapFont schrift;
    private float leer;
    private int felderaktiv;

    public MiniGame3(PoliticCrash game, SpriteBatch batch) {
        this.game = game;
        this.batch = batch;

        hintergrund= new Texture("InGame/Minispiel/Layout_Landkarte_dunkel.png");
        schrift = new BitmapFont();
        screenfase =0;
        sr = new ShapeRenderer();
        rnd = new Random();
        felderaktiv=0;
        figur= new int[5][5];
        for (int i = 0; i < figur.length; i++) {
            for (int j = 0; j < figur[i].length; j++) {
                figur[i][j] = 0;
            }
        }
        bild= new int[5][5];
        for (int i = 0; i < bild.length; i++) {
            for (int j = 0; j < bild[i].length; j++) {
                bild[i][j] = 0;
            }
        }

        //zufälliges erstellen einer Figur aus 6 Feldern
        for(int i=0;i<6;i++){
            int rand1;
            int rand2;
            do {
                rand1 = (rnd.nextInt(figur.length)+0);
                rand2 = (rnd.nextInt(figur.length)+0);
            }while(figur[rand1][rand2] == 1);
            figur[rand1][rand2]=1;
        }

        linen= new float[2][figur[1].length+1];
        float tabGr= (float) (Gdx.graphics.getHeight()-Gdx.graphics.getHeight()*0.1);

        //vorbereitung Dynamisches Koordinatensysem
        leer=tabGr/figur.length;
        Vector2 tabStart= new Vector2((Gdx.graphics.getWidth()-tabGr)/2,(Gdx.graphics.getHeight()-tabGr)/2);
        tabEnde= new Vector2(tabStart.x+tabGr,tabStart.y+tabGr);
        for(int i=0;i<linen[0].length;i++){
            linen[0][i]=tabStart.x;
            tabStart.x=tabStart.x+leer;
        }
        for(int i=0;i<linen[1].length;i++){
            linen[1][i]=tabStart.y;
            tabStart.y=tabStart.y+leer;

        }
    }

    public void muster(){
        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.setColor(166f/255f,255f/255f,166f/255f,1);
        for (int i=0;i<figur.length;i++){
            for (int j=0;j<figur[i].length;j++){
                if(figur[i][j]==1){
                    sr.rect(linen[0][j],linen[1][figur[i].length-i-1],leer,leer);
                }
            }
        }
        sr.end();
    }

    public void bildzeichnen(){
        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.setColor(166,255,255,1);
        for (int i=0;i<bild.length;i++){
            for (int j=0;j<bild[i].length;j++){
                if(bild[i][j]==1){
                    sr.rect(linen[0][j],linen[1][figur[i].length-i-1],leer,leer);
                }
            }
        }
        sr.end();
    }

    public void spielfeld(){
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.setColor(126,126,128,1);
        for(int i=0;i<linen[0].length;i++){
            sr.line(linen[0][i],linen[1][0],linen[0][i],tabEnde.y);
        }
         for(int i=0;i<linen[1].length;i++){
             sr.line(linen[0][0],linen[1][i],tabEnde.x,linen[1][i]);
        }
        sr.end();
    }

    public boolean zwischen(float x,float unten, float oben){
        return unten <= x && x <= oben;
    }

    public void bildauswahl(int x,int y){
        if(zwischen(x,linen[0][0],linen[0][1])){
            if(zwischen(y,linen[1][0],linen[1][1])){bild[0][0]=1;}
            if(zwischen(y,linen[1][1],linen[1][2])){bild[1][0]=1;}
            if(zwischen(y,linen[1][2],linen[1][3])){bild[2][0]=1;}
            if(zwischen(y,linen[1][3],linen[1][4])){bild[3][0]=1;}
            if(zwischen(y,linen[1][4],linen[1][5])){bild[4][0]=1;}
        }
        if(zwischen(x,linen[0][1],linen[0][2])){
            if(zwischen(y,linen[1][0],linen[1][1])){bild[0][1]=1;}
            if(zwischen(y,linen[1][1],linen[1][2])){bild[1][1]=1;}
            if(zwischen(y,linen[1][2],linen[1][3])){bild[2][1]=1;}
            if(zwischen(y,linen[1][3],linen[1][4])){bild[3][1]=1;}
            if(zwischen(y,linen[1][4],linen[1][5])){bild[4][1]=1;}
        }
        if(zwischen(x,linen[0][2],linen[0][3])){
            if(zwischen(y,linen[1][0],linen[1][1])){bild[0][2]=1;}
            if(zwischen(y,linen[1][1],linen[1][2])){bild[1][2]=1;}
            if(zwischen(y,linen[1][2],linen[1][3])){bild[2][2]=1;}
            if(zwischen(y,linen[1][3],linen[1][4])){bild[3][2]=1;}
            if(zwischen(y,linen[1][4],linen[1][5])){bild[4][2]=1;}
        }
        if(zwischen(x,linen[0][3],linen[0][4])){
            if(zwischen(y,linen[1][0],linen[1][1])){bild[0][3]=1;}
            if(zwischen(y,linen[1][1],linen[1][2])){bild[1][3]=1;}
            if(zwischen(y,linen[1][2],linen[1][3])){bild[2][3]=1;}
            if(zwischen(y,linen[1][3],linen[1][4])){bild[3][3]=1;}
            if(zwischen(y,linen[1][4],linen[1][5])){bild[4][3]=1;}
        }
        if(zwischen(x,linen[0][4],linen[0][5])){
            if(zwischen(y,linen[1][0],linen[1][1])){bild[0][4]=1;}
            if(zwischen(y,linen[1][1],linen[1][2])){bild[1][4]=1;}
            if(zwischen(y,linen[1][2],linen[1][3])){bild[2][4]=1;}
            if(zwischen(y,linen[1][3],linen[1][4])){bild[3][4]=1;}
            if(zwischen(y,linen[1][4],linen[1][5])){bild[4][4]=1;}
        }
    }

    @Override
    public void render(float delta) {

        int w=Gdx.graphics.getWidth(); int h=Gdx.graphics.getHeight();
        batch.begin();
        batch.draw(hintergrund, 0, 0, w, h);
        batch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){

            screenfase=1;
        }
        switch (screenfase) {
            case 0:
                batch.begin();
                schrift.draw(batch,"Los mit Leertaste",Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()-10);
                batch.end();
                muster();
                break;
            default:
                if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
                    if(felderaktiv<6){
                        int x = Gdx.input.getX();
                        int y = Gdx.input.getY();
                        bildauswahl(x,y);
                    }
                    felderaktiv++;
                }
                if (felderaktiv>=6){
                    int check=0;
                    for(int i=0;i<bild.length;i++){
                        if(Arrays.equals(bild[i],figur[i])){check=check+1;}
                    }
                    if (check==bild.length) {
                        Land l = Land.values()[(int)(Math.random()*7)]; //Zufälliges Land
                        Land.spieler.informationenAendern(25,l); //Informationen hinzufügen
                        Nachrichtsystem.spezialNachricht("Du konntest 25 Informationen von "+l.getName()+" sicherstellen.");
                    }
                    else Nachrichtsystem.spezialNachricht("Du konntest keinerlei Informationen gewinnen.");
                    game.setScreen(game.getInGame()); //Rückkehr
                }
                bildzeichnen();
                break;
        }
        spielfeld();
    }


    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.dispose();
    }
}
