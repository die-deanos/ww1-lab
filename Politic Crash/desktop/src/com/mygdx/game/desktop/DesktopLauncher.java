package com.mygdx.game.desktop;

import com.badlogic.gdx.Files;
import com.mygdx.game.PoliticCrash;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.PoliticCrash;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1920;
		config.height = 1080;
		config.fullscreen = true;
		config.resizable = false;
		config.vSyncEnabled = true;
		config.title= "Data Heads";
		config.addIcon("InGame\\Icon128.png", Files.FileType.Internal);
		config.addIcon("InGame\\Icon32.png", Files.FileType.Internal);
		config.addIcon("InGame\\Icon16.png", Files.FileType.Internal);
		new LwjglApplication(new PoliticCrash(), config);
	}
}
